/*
 * Created by: Byron Marohn and Michael Matthews, Intel Corporation
 * This file for use specifically by the 2015 Summer GPU computing class at Portland State University.
 * All rights reserved by Intel Corporation.
 */

#ifndef __TIME_PROFILING_H__
#define __TIME_PROFILING_H__

#ifdef USE_TIME_PROFILING

#include <stdio.h>
#include <time.h>


#define COMPILER_BARRIER() asm volatile("" ::: "memory")

//
// Number of nanoseconds in one second.
//
#define NANOSECONDS_PER_SECOND 1000000000LL

/**
 * File handle which will be used to write benchmarking data to
 */
static FILE* benchmark_file = NULL;


/** Opens a file to append benchmarking data to if one is not already open
 *  \param filename File to open for appending benchmark data to
 */
//#define BENCHMARK_INIT(filename)
#define BENCHMARK_INIT() \
	struct timespec wallclock_start_time, thread_start_time; \
	struct timespec wallclock_stop_time, thread_stop_time; \
	if(!benchmark_file) benchmark_file = stdout

/** Closes the benchmarking file, flushing all pending writes */
#define BENCHMARK_FINALIZE() \
	benchmark_file = NULL

/** Declares timer variables and gets the starting time of the function
 *  Should be placed directly after declarations in function to benchmark
 */
#define BENCHMARK_START() \
	COMPILER_BARRIER(); \
	(void)clock_gettime( CLOCK_MONOTONIC, &wallclock_start_time ); \
	(void)clock_gettime( CLOCK_THREAD_CPUTIME_ID, &thread_start_time ); \
	COMPILER_BARRIER()

/** Measures the finishing time of the function, computes the elapsed time, and writes it to a file
 *  \param key           String to use to identify data from this function, first thing printed in the log
 *  \param problemsize   Unsigned long integer (%lu) to describe how big the input to the function was.
 *                         Must be specified, but need not make sense (could just be 0)
 */
#define BENCHMARK_STOP(key) \
	COMPILER_BARRIER(); \
	(void)clock_gettime( CLOCK_MONOTONIC, &wallclock_stop_time ); \
	(void)clock_gettime( CLOCK_THREAD_CPUTIME_ID, &thread_stop_time ); \
	if(benchmark_file) { \
		(void)fprintf( benchmark_file, "%s %lld\n", \
		               key, \
		               ( ( (long long)( wallclock_stop_time.tv_sec - wallclock_start_time.tv_sec ) * NANOSECONDS_PER_SECOND ) + (long long)( wallclock_stop_time.tv_nsec - wallclock_start_time.tv_nsec ) ) ); \
	} \
	COMPILER_BARRIER()

#else /* USE_TIME_PROFILING */

#define BENCHMARK_INIT()
#define BENCHMARK_FINALIZE()
#define BENCHMARK_START(max_current_time_count_in)
#define BENCHMARK_STOP(key)

#endif /* USE_TIME_PROFILING */

#endif /* __TIME_PROFILING_H__ */


