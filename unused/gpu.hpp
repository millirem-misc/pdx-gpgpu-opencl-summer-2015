#ifndef GPU__H

#define GPU__H

#define TILE_WIDTH 32

#include <ctime>
#include <string>

#ifdef OPENCL__CODE

#include <CL/cl.hpp>

#endif

typedef unsigned int uint;

template <typename T>
class GPUTest {

public:

  T* filter;
  T* dev_filter;
  T* image;
  T* dev_image;
  T* dev_output;

  uint filter_width;
  uint im_x;
  uint im_y;

  bool is_ready;

#ifdef OPENCL__CODE


  cl::Context context;
  cl::Program program;
  cl::Kernel kernel;
  cl::Buffer image_buf;
  cl::Buffer filter_buf;

#endif

  std::string _device = "";
  std::string _version = "";

  GPUTest(uint fwidth = 5, uint x = 1920, uint y = 1080);
  ~GPUTest();

  bool setup();

  const std::string& device() const { return _device; }
  const std::string& version() const { return _version; }
  
  void run();

  void timer_start();
  clock_t timer_end();

};

#ifdef CUDA__CODE

#include "cuda_code.hpp"

#endif

#ifdef OPENCL__CODE

#include "opencl_code.hpp"

#endif

void gpu_startup();

void gpu_run();

void gpu_teardown();


#endif
