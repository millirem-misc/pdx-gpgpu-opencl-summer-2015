#ifndef CUDA__CONVOL__HPP

#define CUDA__CONVOL__HPP


#include <cuda_runtime.h>

#define MAX_TILE_WIDTH 32

#ifndef CUDA_CONST_MEM

extern __constant__ int* cfilter;
extern __constant__ uint cfilter_width;

#endif

void call_kernel(int grids, int threads, int* image, uint im_x, uint im_y, uint tile_width);

template <typename T>
void call_kernel_template(int grids, int threads, T* filter, T* image, T* output, uint im_x, uint im_y, uint filter_width);

template <typename T>
__global__ void convol_kernel_template(T* filter, T* image, T* output, uint im_x, uint im_y, uint filter_width) {

  //  uint x = blockIdx.x * blockDim.x + threadIdx.x;
  //uint y = blockIdx.y * blockDim.y + threadIdx.y;

}

#endif
