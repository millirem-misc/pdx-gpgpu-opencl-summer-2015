#include <stdio.h> // for printf
#define TILE_WIDTH 32
typedef int DATA_TYPE;

void fill_data(DATA_TYPE *in_matrix, int MAT_SIZE)
{
    int i;
    for (i=0; i<MAT_SIZE*MAT_SIZE; i++)
        in_matrix[i] = i;
}

void print_data(FILE *fp,DATA_TYPE *in_matrix,int MAT_SIZE)
{
    int x, y;
    for (y=MAT_SIZE-1; y>=0; y--) {
        for (x=0; x<MAT_SIZE; x++) {
            fprintf(fp,"   %d", in_matrix[y * MAT_SIZE + x]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"-----\n");
}


__global__ void matrixMultiply_kernel(DATA_TYPE * A, DATA_TYPE * B, DATA_TYPE * C,
                               int MAT_SIZE) {
    __shared__ DATA_TYPE ds_M[TILE_WIDTH][TILE_WIDTH];
    __shared__ DATA_TYPE ds_N[TILE_WIDTH][TILE_WIDTH];
    int bx = blockIdx.x, by = blockIdx.y,
    tx = threadIdx.x, ty = threadIdx.y,
    Row = by * TILE_WIDTH + ty,
    Col = bx * TILE_WIDTH + tx;
    DATA_TYPE Pvalue = 0;
    
    for (int m = 0; m < MAT_SIZE/TILE_WIDTH; ++m) {
        ds_M[ty][tx] = A[Row*MAT_SIZE + m*TILE_WIDTH+tx];
        ds_N[ty][tx] = B[(m*TILE_WIDTH+ty)*MAT_SIZE+Col];
        
        __syncthreads();
        for (int k = 0; k < TILE_WIDTH; ++k)
            Pvalue += ds_M[ty][k] * ds_N[k][tx];
        __syncthreads();
    }
        C[Row*MAT_SIZE+Col] = Pvalue;
}

void matmult_dev(DATA_TYPE * A, DATA_TYPE * B, DATA_TYPE * C,
                  int MAT_SIZE)
{
    // Step 1: Allocate memory
    DATA_TYPE *A_dev, *B_dev, *C_dev;
    cudaMalloc((void **) &A_dev, sizeof(DATA_TYPE) * (MAT_SIZE*MAT_SIZE));
    cudaMalloc((void **) &B_dev, sizeof(DATA_TYPE) * (MAT_SIZE*MAT_SIZE));
    cudaMalloc((void **) &C_dev, sizeof(DATA_TYPE) * (MAT_SIZE*MAT_SIZE));
    
    // Step 2: Copy the input vectors to the device
    cudaMemcpy(A_dev, A, sizeof(DATA_TYPE) * (MAT_SIZE*MAT_SIZE), cudaMemcpyHostToDevice);
    cudaMemcpy(B_dev, B, sizeof(DATA_TYPE) * (MAT_SIZE*MAT_SIZE), cudaMemcpyHostToDevice);
    
    // Step 3: Invoke the kernel
    int block_x,block_y;
    block_x=MAT_SIZE/TILE_WIDTH;
    if(MAT_SIZE%TILE_WIDTH!=0) block_x++;
    block_y=MAT_SIZE/TILE_WIDTH;
    if(MAT_SIZE%TILE_WIDTH!=0) block_y++;
    
    dim3 dimGrid(block_x,block_y,  1);
    dim3 dimBlock(TILE_WIDTH,TILE_WIDTH,1);
    matrixMultiply_kernel<<<dimGrid, dimBlock>>>(A_dev, B_dev, C_dev,MAT_SIZE);
    
    // Step 4: Retrieve the results
    cudaMemcpy(C, C_dev, sizeof(DATA_TYPE) * (MAT_SIZE*MAT_SIZE), cudaMemcpyDeviceToHost);
    
    // Step 5: Free device memory
    cudaFree(A_dev);
    cudaFree(B_dev);
    cudaFree(C_dev);
}


void matmult_host(DATA_TYPE* A, DATA_TYPE* B, DATA_TYPE* C, int MAT_SIZE)
{
    for (int i = 0; i < MAT_SIZE; ++i)
    {
        for (int j = 0; j < MAT_SIZE; ++j) {
            DATA_TYPE sum = 0;
            for (int k = 0; k < MAT_SIZE; ++k) {
                DATA_TYPE a = A[i * MAT_SIZE + k];
                DATA_TYPE b = B[k * MAT_SIZE + j];
                sum += a * b;
            }
            C[i * MAT_SIZE + j] = sum;
        }
}
}

int main(int argc, char ** argv) {
    int MAT_SIZE; //
    if(argc < 2)
    {
        printf (" The format is exe WIDTH HEIGHT");
        exit(1);
    }
    MAT_SIZE = atoi(argv[1]);

    DATA_TYPE *A=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(MAT_SIZE*MAT_SIZE));
    DATA_TYPE *B=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(MAT_SIZE*MAT_SIZE));
    DATA_TYPE *hostC=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(MAT_SIZE*MAT_SIZE));
    DATA_TYPE *deviceC=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(MAT_SIZE*MAT_SIZE));
    
    fill_data(A,MAT_SIZE);
    //print_data(stdout,A,MAT_SIZE);
                                            
    fill_data(B,MAT_SIZE);
    //print_data(stdout,B,MAT_SIZE);
    
    matmult_host(A,B,hostC,MAT_SIZE);
    printf("CPU done\n");
    //print_data(stdout,hostC,MAT_SIZE);
                                            
    matmult_dev(A,B,deviceC,MAT_SIZE);
    printf("GPU done\n");
    
    //print_data(stdout,deviceC,MAT_SIZE);
    
    if(memcmp(hostC, deviceC, sizeof(DATA_TYPE)*(MAT_SIZE*MAT_SIZE)) != 0) {
        printf("Error: Host and GPU differ\n");
    } else {
        printf("Success: Host and GPU computed same results\n");
    }

    
    free(A);
    free(B);
    free(hostC);
    free(deviceC);
 
    return 0;
}
