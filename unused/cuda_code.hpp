#include "gpu.hpp"

#include "cuda_convol.hpp"

cudaError_t handle_gpu_error(const char* desc, const cudaError_t status);

template <typename T>
GPUTest<T>::GPUTest(uint fwidth, uint x, uint y):
 filter(nullptr),
 dev_filter(nullptr),
 image(nullptr),
 dev_image(nullptr),
 dev_output(nullptr),
 filter_width(fwidth),
 im_x(x),
 im_y(y),
 is_ready(false) {}

template <typename T>
GPUTest<T>::~GPUTest() {

  if (nullptr != filter) {
    delete[] filter;
  }

  if (nullptr != image) {
    delete[] image;
  }

  if (nullptr != dev_filter) {
    cudaFree(dev_filter);
  }

  if (nullptr != dev_image) {
    cudaFree(dev_image);
  }

  if (nullptr != dev_output) {
    cudaFree(dev_output);
  }

}

template <typename T>
bool GPUTest<T>::setup() {

  cudaError_t status = cudaSuccess;

  image = new T[im_x * im_y]();

  filter = new T[filter_width * filter_width]();

  if (handle_gpu_error("cudaMalloc for dev_filter", cudaMalloc(&dev_filter, filter_width * filter_width * sizeof(T))) != cudaSuccess) {
    return is_ready;
  }

  if (handle_gpu_error("cudaMalloc for dev_image", cudaMalloc(&dev_image, im_x * im_y * sizeof(T))) != cudaSuccess) {
    return is_ready;
  }

  if (handle_gpu_error("cudaMalloc for dev_output", cudaMalloc(&dev_output, im_x * im_y * sizeof(T))) != cudaSuccess) {
    return is_ready;
  }

  cudaDeviceProp prop;

  if (handle_gpu_error("cudaGetDeviceProperties", cudaGetDeviceProperties(&prop, 0)) != cudaSuccess) {
      return is_ready;
  }

  _device = prop.name;
  _version = "CUDA " + std::to_string(prop.major) + "." + std::to_string(prop.minor);

  is_ready = status == cudaSuccess;

  return is_ready;
}

template <typename T>
void GPUTest<T>::run() {
  call_kernel_template<T>(1, 512, dev_filter, dev_image, dev_output, im_x, im_y, filter_width);
}

template <typename T>
void GPUTest<T>::timer_start() {

}

template <typename T>
clock_t GPUTest<T>::timer_end() {
  return 0;
}

