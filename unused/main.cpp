#include <iostream>
#include <cstdlib>
#include <limits>
#include <stdexcept>

#include "gpu.hpp"

template <typename T>
bool run_tests(uint x, uint y, uint fwidth);

uint stou(const std::string& str, size_t * idx = 0, int base = 10);

int main(int argc, char** argv) {

  if (argc != 1 && argc != 5) {
    std::cout << "Especting 0 or 4 arguments, received " << argc << " instead." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  uint x = (argc == 1) ? 1920 : stou(argv[2]);
  uint y = (argc == 1) ? 1080 : stou(argv[3]);
  uint fwidth = (argc == 1) ? 5 : stou(argv[4]);

  bool isFloat = (argc == 1) ? true : (argv[1])[0] == 'f';

  bool success = (isFloat) ? run_tests<float>(x, y, fwidth) : run_tests<int>(x, y, fwidth);

  if (success) {
    std::exit(EXIT_SUCCESS);
  } else {
    std::exit(EXIT_FAILURE);
  }

}

uint stou(std::string const & str, size_t * idx, int base) {
  unsigned long result = std::stoul(str, idx, base);
  if (result > std::numeric_limits<unsigned>::max()) {
    throw std::out_of_range("stou");
  }
  return result;
}

template <typename T>
bool run_tests(uint x, uint y, uint fwidth) {

  auto gpu = GPUTest<T>(x, y, fwidth);

  std::cout << "setting up gpu" << std::endl;

  bool ready = gpu.setup();

  if (ready) {
    std::cout << "Device: " << gpu.device() << std::endl;
    std::cout << "Version: " << gpu.version() << std::endl;

    std::cout << "running gpu convolution kernel" << std::endl;

    gpu.run();

  // teardown will occur as the destructor is invoked
    std::cout << "tearing down gpu runtime" << std::endl;

  //  gpu_teardown();

    return true;

  } else {

    std::cout << "setup failed, exiting" << std::endl;

    return true;
  }

}
