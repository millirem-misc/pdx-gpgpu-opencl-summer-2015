#include "gpu.hpp"

#include <CL/cl.hpp>

#include <iostream>

void checkErr(cl_int, const char*);

void gpu_startup() {
  cl_int err;
  std::vector< cl::Platform > platformList;

  cl::Platform::get(&platformList);

  checkErr(platformList.size()!=0 ? CL_SUCCESS : -1, "cl::Platform::get");

  std::cerr << "Platform number is: " << platformList.size() << std::endl;

  std::string platformVendor;
  std::string platformVersion;

  platformList[0].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);

  std::cerr << "Platform is by: " << platformVendor << std::endl;

  platformList[0].getInfo((cl_platform_info)CL_PLATFORM_VERSION, &platformVersion);

  std::cerr << "Platform version: " << platformVersion << std::endl;

  cl_context_properties cprops[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)(platformList[0])(), 0};

cl::Context context(CL_DEVICE_TYPE_GPU,
		    cprops,
		    NULL,
		    NULL,
		    &err);

 for (uint i = 0; i < 3; ++i) {
   std::cout << cprops[i] << std::endl;
 }

 checkErr(err, "Context::Context()");

}

void gpu_run() {
}

void gpu_teardown() {
}

inline void checkErr(cl_int err, const char * name) {
  if (err != CL_SUCCESS) {
    std::cerr << "ERROR: " << name  << " (" << err << ")" << std::endl;
    exit(EXIT_FAILURE);
  }
}
