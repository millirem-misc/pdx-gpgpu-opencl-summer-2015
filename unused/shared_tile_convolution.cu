#include <stdio.h> // for printf

typedef int DATA_TYPE ;
int WIDTH;
int HEIGHT;
const int TILE_WIDTH=32;
const int MASK_WIDTH=5;

void fill_data(DATA_TYPE *in_matrix, int WIDTH, int HEIGHT)
{
    int i;
    for (i=0; i<WIDTH*HEIGHT; i++)
        in_matrix[i] = i;
}

void print_data(FILE *fp,DATA_TYPE *in_matrix,int WIDTH,int HEIGHT)
{
    int x, y;
    for (y=HEIGHT-1; y>=0; y--) {
        for (x=0; x<WIDTH; x++) {
            fprintf(fp,"   %d", in_matrix[y * WIDTH + x]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"-----\n");
}



__global__ void convolute_kernel(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix, int WIDTH,int HEIGHT)
{
    int x,y;
    int out_index;
    int in_x_start,in_y_start;
    DATA_TYPE out_value;

    y = blockIdx.y * blockDim.y + threadIdx.y;
    x = blockIdx.x * blockDim.x + threadIdx.x;
    if(x<WIDTH && y<HEIGHT)
    {
        out_index = (y*WIDTH)+x;
            in_x_start=x-MASK_WIDTH/2;
            in_y_start=y-MASK_WIDTH/2;
            out_value=0;
            for(int j=0; j<MASK_WIDTH; j++)
            {
                for (int k=0; k<MASK_WIDTH; k++) {
                    if(in_x_start+k >=0 && in_y_start+j >=0 && in_x_start+k < WIDTH && in_y_start+j < HEIGHT)
                        out_value+=in_matrix[((in_y_start+j)*WIDTH)+in_x_start+k]*mask_matrix[j*MASK_WIDTH +k];
                    
                }
            }
            out_matrix[out_index]=out_value;
    }
}

__global__ void convolute_shared_kernel(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix, int WIDTH,int HEIGHT)
{
    int x,y;
    int out_index;
    int in_x_start,in_y_start;
    DATA_TYPE out_value;
    int n=MASK_WIDTH/2;
    __shared__ DATA_TYPE N_ds[TILE_WIDTH+MASK_WIDTH-1][TILE_WIDTH+MASK_WIDTH-1];
    
    y = blockIdx.y * blockDim.y + threadIdx.y;
    x = blockIdx.x * blockDim.x + threadIdx.x;
    if(x<WIDTH && y<HEIGHT)
    {
        in_x_start=x-n;
        in_y_start=y-n;
        out_index = (y*WIDTH)+x;
        out_value=0;
        
        // Loading bottom halo
        if( threadIdx.y < n)
        {
            N_ds[threadIdx.y][threadIdx.x+n]=in_y_start<0?0:in_matrix[(in_y_start*WIDTH)+x];
            //printf(" In bottom x is %d y is %d blockIdx.x is %d blockIdx.y is %d  N_ds[%d][%d] is %d \n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,threadIdx.y, threadIdx.x+n, N_ds[threadIdx.y][threadIdx.x+n]);
	  
        }
        // Loading top halo
        else if( threadIdx.y >= blockDim.y-n)
        {
            N_ds[threadIdx.y+n+n][threadIdx.x+n]=(y+n)>=HEIGHT?0:in_matrix[((y+n)*WIDTH)+x];
            //printf(" In top y+n is %d HEIGHT is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d  \n",y+n, HEIGHT,blockIdx.x,blockIdx.y,threadIdx.y+n+n, threadIdx.x+n, N_ds[threadIdx.y+n+n][threadIdx.x+n]);
        }
        // Loading left halo
        if(threadIdx.x <n)
        {
            N_ds[threadIdx.y+n][threadIdx.x]=in_x_start<0?0:in_matrix[(y*WIDTH)+in_x_start];
            //printf(" In left x is %d y is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d  \n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,threadIdx.y+n, threadIdx.x, N_ds[threadIdx.y+n][threadIdx.x]);
        }
        //Loading right halo
        else if(threadIdx.x >= blockDim.x-n )
        {
            N_ds[threadIdx.y+n][threadIdx.x+n+n]=(x+n)>=WIDTH?0:in_matrix[(y*WIDTH)+x+n];
            //printf(" In right x is %d y is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d\n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,threadIdx.y+n, threadIdx.x+n+n, N_ds[threadIdx.y+n][threadIdx.x+n+n]);
        }
        // Load left lower corner
        if(threadIdx.x == 0 && threadIdx.y ==0)
        {
            for(int my=0;my<n;my++)
            {
                for( int mx=0;mx<n;mx++)
                {
                    N_ds[my][mx]= (in_x_start <0 || in_y_start <0 )? 0 : in_matrix[((in_y_start+my)*WIDTH)+in_x_start+mx];
            		//printf(" In lower left corner x is %d y is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d\n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,my, mx, N_ds[my][mx]);
                }
            }
        }
        // Load left upper corner
        else if (threadIdx.x == 0 && threadIdx.y == blockDim.y-1)
        {
	    int y_pos=y+1;
            for(int my=threadIdx.y+n+1; my < (threadIdx.y+n+n+1); my++)
            {
                for( int mx=0;mx<n;mx++)
                {
                    N_ds[my][mx]= (in_x_start <0 || y+n >= HEIGHT )? 0 : in_matrix[((y_pos)*WIDTH)+in_x_start+mx];
           	    //printf(" In upper left corner x is %d y is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d\n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,my, mx, N_ds[my][mx]);
                }
		y_pos++;
            }
        }
        // Load right lower corner
        else if(threadIdx.y == 0 && threadIdx.x ==blockDim.x-1)
        {
            for(int my=0;my<n;my++)
            {
                int x_pos = x+1;
                for( int mx=threadIdx.x+n+1;mx<threadIdx.x+n+n+1;mx++)
                {
                    N_ds[my][mx]= (x+n >= WIDTH || in_y_start <0) ? 0 : in_matrix[((in_y_start+my)*WIDTH)+x_pos];
           	    //printf(" In lower right corner x is %d y is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d\n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,my, mx, N_ds[my][mx]);
		x_pos++;
                }
            }
        }
        // Load right upper corner
        else if(threadIdx.y == blockDim.y -1 && threadIdx.x ==blockDim.x-1)
        {
            int y_pos = y+1;
            for(int my=threadIdx.y+n+1;my<threadIdx.y+n+n+1;my++)
            {
                int x_pos = x+1;
                for( int mx=threadIdx.x+n+1;mx<threadIdx.x+n+n+1;mx++)
                {
                    N_ds[my][mx]= ( x+n >= WIDTH || y+n >= HEIGHT) ? 0 : in_matrix[((y_pos)*WIDTH)+(x_pos++)];
           	    //printf(" In upper right corner x is %d y is %d blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d\n",threadIdx.x, threadIdx.y,blockIdx.x,blockIdx.y,my, mx, N_ds[my][mx]);
                }
		y_pos++;
            }
        }
	// Load each threads element
        N_ds[threadIdx.y+n][threadIdx.x+n]= in_matrix[(y*WIDTH)+x];
	//printf(" blockIdx.x is %d blockIdx.y is %d N_ds[%d][%d] is %d  \n",blockIdx.x, blockIdx.y, threadIdx.y+n, threadIdx.x+n, N_ds[threadIdx.y+n][threadIdx.x+n]);
	__syncthreads();

        for(int j=0; j<MASK_WIDTH; j++)
        {
            for (int k=0; k<MASK_WIDTH; k++) {
                    out_value+=N_ds[threadIdx.y+j][threadIdx.x+k]*mask_matrix[j*MASK_WIDTH +k];
                
            }
        }
	__syncthreads();
        out_matrix[out_index]=out_value;
    }
}

// void convolute(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix, int WIDTH,int HEIGHT)
void convolute_dev(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix, int WIDTH,int HEIGHT)
{
 // Step 1: Allocate memory
     DATA_TYPE *in_matrix_dev, *out_matrix_dev, *mask_matrix_dev;
 cudaMalloc((void **) &in_matrix_dev, sizeof(DATA_TYPE) * (WIDTH*HEIGHT));
 cudaMalloc((void **) &out_matrix_dev, sizeof(DATA_TYPE) * (WIDTH*HEIGHT));
 cudaMalloc((void **) &mask_matrix_dev, sizeof(DATA_TYPE) * (MASK_WIDTH*MASK_WIDTH));
 
 // Step 2: Copy the input vectors to the device
 cudaMemcpy(in_matrix_dev, in_matrix, sizeof(DATA_TYPE) * (WIDTH*HEIGHT), cudaMemcpyHostToDevice);
 cudaMemcpy(mask_matrix_dev, mask_matrix, sizeof(DATA_TYPE) * (MASK_WIDTH*MASK_WIDTH), cudaMemcpyHostToDevice);
 
 // Step 3: Invoke the kernel
 int block_x,block_y;
 block_x=WIDTH/TILE_WIDTH;
 if(WIDTH%TILE_WIDTH!=0) block_x++;
 block_y=HEIGHT/TILE_WIDTH;
 if(HEIGHT%TILE_WIDTH!=0) block_y++;
 printf (" Num blocks is %d x is %d in y is %d \n",(block_x*block_y),block_x,block_y);
 
 dim3 dimGrid(block_x,block_y,  1);
 dim3 dimBlock(TILE_WIDTH,TILE_WIDTH,1);
 //convolute_kernel<<<dimGrid, dimBlock>>>(out_matrix_dev, in_matrix_dev, mask_matrix_dev,WIDTH,HEIGHT);
 convolute_shared_kernel<<<dimGrid, dimBlock>>>(out_matrix_dev, in_matrix_dev, mask_matrix_dev,WIDTH,HEIGHT);
 
     // Step 4: Retrieve the results
 cudaMemcpy(out_matrix, out_matrix_dev, sizeof(DATA_TYPE) * (WIDTH*HEIGHT), cudaMemcpyDeviceToHost);
 
 // Step 5: Free device memory
 cudaFree(in_matrix_dev);
 cudaFree(mask_matrix_dev);
 cudaFree(out_matrix_dev);
 }



void convolute(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix, int WIDTH,int HEIGHT) {
    int x, y;
    int out_index;
    int in_x_start,in_y_start;
    DATA_TYPE out_value;
    for (y=0; y<HEIGHT; y++) {
        for (x=0; x<WIDTH; x++) {
            out_index = (y*WIDTH)+x;
            in_x_start=x-MASK_WIDTH/2;
            in_y_start=y-MASK_WIDTH/2;
            out_value=0;
	    //printf("x start is %d , y start is %d \n",in_x_start, in_y_start);
            for(int j=0; j<MASK_WIDTH; j++)
            {
                for (int k=0; k<MASK_WIDTH; k++) {
                    if(in_x_start+k >=0 && in_y_start+j >=0 && in_x_start+k < WIDTH && in_y_start+j < HEIGHT)
                        out_value+=in_matrix[((in_y_start+j)*WIDTH)+in_x_start+k]*mask_matrix[j*MASK_WIDTH +k];

                }
            }
            out_matrix[out_index]=out_value;
        }
    }


}


int main(int argc, char *argv[])
{
    if(argc < 2)
    {
	printf (" The format is exe WIDTH HEIGHT\n");
	exit(1);
    }
    WIDTH = atoi(argv[1]);
    HEIGHT = atoi(argv[2]);
    DATA_TYPE *in_matrix=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(WIDTH*HEIGHT));
    DATA_TYPE *out_matrix_host=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(WIDTH*HEIGHT));
    DATA_TYPE *out_matrix_dev=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(WIDTH*HEIGHT));
    DATA_TYPE *mask_matrix=(DATA_TYPE *)malloc(sizeof(DATA_TYPE)*(MASK_WIDTH*MASK_WIDTH));

    FILE *host_file = stdout;
    FILE *dev_file=stdout;
    /*
    FILE *host_file=fopen("host_result","w");
    FILE *dev_file=fopen("dev_result","w");
    if(host_file==NULL){
        fprintf(stderr,"Cannot open file host_result\n");
        exit(1);
    }
    if(dev_file==NULL){
        fprintf(stderr,"Cannot open file dev_result\n");
        exit(1);
    }
    */
    fill_data(in_matrix,WIDTH,HEIGHT);
    //print_data(host_file,in_matrix,WIDTH,HEIGHT);

    fill_data(mask_matrix,MASK_WIDTH,MASK_WIDTH);
    //print_data(host_file,mask_matrix,MASK_WIDTH,MASK_WIDTH);

    convolute(out_matrix_host,in_matrix,mask_matrix,WIDTH,HEIGHT);
    printf("CPU done\n");
    //print_data(host_file,out_matrix_host,WIDTH,HEIGHT);

    convolute_dev(out_matrix_dev,in_matrix,mask_matrix,WIDTH,HEIGHT);
    printf("GPU done\n");
    //print_data(dev_file,out_matrix_dev,WIDTH,HEIGHT);
	if(memcmp(out_matrix_host, out_matrix_dev, sizeof(DATA_TYPE)*(WIDTH*HEIGHT)) != 0) {
		printf("Error: Host and GPU differ\n");
	} else {
		printf("Success: Host and GPU computed same results\n");
	}

    fclose(host_file);
    fclose(dev_file);
    free(in_matrix);
    free(out_matrix_host);
    free(out_matrix_dev);
    free(mask_matrix);

return 0;
}
