#include "gpu.hpp"
#include "cuda_convol.hpp"

#include <iostream>

int* filter = nullptr;
int* image = nullptr;
uint filter_width = 3;
uint im_x = 1920;
uint im_y = 1080;
uint tile_width = 16;

int* dev_filter = nullptr;
int* dev_image = nullptr;

bool ready = false;

cudaError_t handle_gpu_error(const char* desc, const cudaError_t status) {

  if (status != cudaSuccess) {
    const char* msg = cudaGetErrorString(status);

    std::cerr << desc
	      << ", error code was "
	      << status
	      << ": "
	      << msg
	      << std::endl;

  }

  return status;
}

void gpu_startup() {

  cudaError_t status = cudaSuccess;

  image = new int[im_x * im_y]();

  filter = new int[filter_width * filter_width]();
  /*
  if (handle_gpu_error("cudaMalloc for dev_filter", cudaMalloc(&dev_filter, filter_width * filter_width * sizeof(int))) != cudaSuccess) {
    return;
  }
  */
  if (handle_gpu_error("cudaMemcpyToSymbol for filter", cudaMemcpyToSymbol(&cfilter, &filter, sizeof(int) * filter_width * filter_width)) != cudaSuccess) {
    return;
  }

  if (handle_gpu_error("cudaMemcpyToSymbol for filter_width", cudaMemcpyToSymbol(&cfilter_width, &filter_width, sizeof(uint) )) != cudaSuccess) {
    return;
  }

  if (handle_gpu_error("cudaMalloc for dev_image", cudaMalloc(&dev_image, im_x * im_y * sizeof(int))) != cudaSuccess) {
    return;
  }

  
  ready = status == cudaSuccess;
}

void gpu_run() {

  if (ready) {
    call_kernel(1, 512, dev_image, im_x, im_y, tile_width);
  }

}

void gpu_teardown() {

  if (nullptr != filter) {
    delete[] filter;
  }

  if (nullptr != image) {
    delete[] image;
  }
  /*
  if (nullptr != dev_filter) {
    cudaFree(dev_filter);
    }*/

  if (nullptr != dev_image) {
    cudaFree(dev_image);
  }

}

