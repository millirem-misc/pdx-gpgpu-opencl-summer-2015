#ifndef OPENCL_CODE__HPP

#define OPENCL__CODE__HPP

template <typename T>
GPUTest<T>::GPUTest(uint fwidth, uint x, uint y) :
  filter(nullptr),
  dev_filter(nullptr),
  image(nullptr),
  dev_image(nullptr),
  dev_output(nullptr),
  filter_width(fwidth),
  im_x(x),
  im_y(y),
  is_ready(false) {}

template <typename T>
GPUTest<T>::~GPUTest() {

  if (nullptr != filter) {
    delete[] filter;
  }

  if (nullptr != image) {
    delete[] image;
  }

  /*  
  if (nullptr != dev_filter) {
    clReleaseMemObject(dev_filter);
  }

  if (nullptr != dev_image) {
    clReleaseMemObject(dev_image);
  }

  if (nullptr != dev_output) {
    clReleaseMemObject(dev_output);
  }
  */
}


template <typename T>
bool GPUTest<T>::setup() {

  filter = new T[filter_width * filter_width]();
  image = new T[im_x * im_y]();

  cl_int status = CL_SUCCESS;

  std::vector<cl::Platform> platform;
  cl::Platform::get(&platform);

  std::vector<cl::Device> device;
  for(auto p = platform.begin(); device.empty() && p != platform.end(); p++) {
    std::vector<cl::Device> pldev;

    try {
      p->getDevices(CL_DEVICE_TYPE_GPU, &pldev);

      for(auto d = pldev.begin(); device.empty() && d != pldev.end(); d++) {
	if (!d->getInfo<CL_DEVICE_AVAILABLE>()) continue;

	device.push_back(*d);
	context = cl::Context(device);
      }
    } catch(...) {
      device.clear();
    }
  }
  
  if (!device.empty()) {
    cl::Device dev = device[0];

    _device = dev.getInfo<CL_DEVICE_NAME>();
    _version = dev.getInfo<CL_DEVICE_OPENCL_C_VERSION>();

    image_buf = cl::Buffer(context, CL_MEM_READ_WRITE, im_x * im_y, image, &status);

  } else {
    std::cerr << "No OpenCL device found." << std::endl;
  }

  is_ready = status == CL_SUCCESS;

  return is_ready;
}

template <typename T>
void GPUTest<T>::run() {

}

template <typename T>
void GPUTest<T>::timer_start() {
}

template <typename T>
clock_t GPUTest<T>::timer_end() {
  return 0;
}

#endif
