#define CUDA_CONST_MEM

#include "cuda_convol.hpp"

__constant__ int* cfilter;
__constant__ uint cfilter_width;

__global__ void convol_kernel(int* image, uint im_x, uint im_y, uint tile_width) {

  //  __shared__ int tiled[MAX_TILE_WIDTH][MAX_TILE_WIDTH];


  uint x = blockIdx.x * blockDim.x + threadIdx.x;
  uint y = blockIdx.y * blockDim.y + threadIdx.y;
  uint i = 0;

  for (; i < tile_width; ++i) {
    image[x*y] = cfilter[i] + cfilter_width;
  }

}

void call_kernel(int grids, int threads, int* image, uint im_x, uint im_y, uint tile_width) {

  convol_kernel<<< grids, threads >>>(image, im_x, im_y, tile_width);

}

template <typename T>
void call_kernel_template(int grids, int threads, T* filter, T* image, T* output, uint im_x, uint im_y, uint filter_width) {

  convol_kernel_template<<< grids, threads >>>(filter, image, output, im_x, im_y, filter_width);

}

template
void call_kernel_template<int>(int, int, int*, int*, int*, uint, uint, uint);

template
void call_kernel_template<float>(int, int, float*, float*, float*, uint, uint, uint);
