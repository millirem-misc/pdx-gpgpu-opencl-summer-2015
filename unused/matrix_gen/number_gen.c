// ECE588 Project
// Fall 2012
// Scott Cline, Yasodhadevi Nachimuthu

#include "project.h"
int main (int argc, char ** argv)
{
   srand(10);
   FILE * matrixA;
   FILE * matrixB;
   int i, j;
   int zero = 0;

   //Below code to generate two more files that aren't square (i.e. 1000x2000, 2000x1500)
   //without filling in zeroes. If you use this, change the file names above to SmatrixA,
   //SmatrixB, and change div_conq and transpose to read in the below file names

   matrixA = fopen("matrixA.txt", "w");
   for (i = 0; i < MATRIX_A_row; i++){
      for (j = 0; j < MATRIX_A_col; j++){
            fprintf(matrixA, "%d ", (rand() % 64)); //Keep the numbers manageable
         if (j == (MATRIX_A_col - 1))
         fprintf(matrixA, "\n");
      }
   }
   fclose(matrixA);

   matrixB = fopen("matrixB.txt", "w");
   for (i = 0; i < MATRIX_B_row; i++){
      for (j = 0; j < MATRIX_B_col; j++){
            fprintf(matrixB, "%d ", (rand() % 64)); //Keep the numbers manageable
         if (j == (MATRIX_B_col - 1))
         fprintf(matrixB, "\n");
      }
   }
   fclose(matrixB);

//Generate two files that represent a square matrix. Generate random numbers
//in the range specified in project.h, and fill in the remaining slots with 0
/*
   matrixA = fopen("matrixA.txt", "w");
   for (i = 0; i < ARRAY_SIZE; i++){
      for (j = 0; j < ARRAY_SIZE; j++){
         if((i > (MATRIX_A_X - 1)) || (j > (MATRIX_A_Y -1 )))
            fprintf(matrixA, "%d ", zero);
         else
            fprintf(matrixA, "%d ", (rand() % 64)); //Keep the numbers manageable
         if (j == (ARRAY_SIZE - 1))
         fprintf(matrixA, "\n");
      }
   }
   fclose(matrixA);

   matrixB = fopen("matrixB.txt", "w");
   for (i = 0; i < ARRAY_SIZE; i++){
      for (j = 0; j < ARRAY_SIZE; j++){
         if((i > (MATRIX_B_X - 1)) || (j > (MATRIX_B_Y -1 )))
            fprintf(matrixB, "%d ", zero);
         else
            fprintf(matrixB, "%d ", (rand() % 64)); //Keep the numbers manageable
         if (j == (ARRAY_SIZE - 1))
         fprintf(matrixB, "\n");
      }
   }
   fclose(matrixB);
*/
  return 0;
}
