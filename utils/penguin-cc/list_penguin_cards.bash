#!/bin/bash

USER=jgm2;
CURRDIR=`pwd`;
HOSTSFILE=penguinhosts

for HOST in $(cat $CURRDIR/$HOSTSFILE); do
	echo -n "$HOST:     ";
        if [ "`ping -c 2 $HOST`" ]; then
		ssh $USER@$HOST $CURRDIR/probe-cardname;
	fi
done

