#include <stdio.h>

int main(void) {
  int *version = new int;
  int deviceCount;
  int device;
  cudaDeviceProp dp;

  cudaDriverGetVersion(version);
  printf("\nCUDA driver version:  %d\n", *version);
  cudaRuntimeGetVersion(version);
  printf("CUDA runtime version:  %d\n\n", *version);

  cudaGetDeviceCount(&deviceCount);
  for (device = 0; device < deviceCount; device++) {
    cudaGetDeviceProperties(&dp, device);
    printf("Device %d has capabilities:\n"
	"Name:  %s\n"
	"Major.Minor:  %d.%d\n"
	"Total global memory:  %ld\n"
        "Max threads per block:  %d\n"
	"Warp size:  %d\n"
	"Clock rate:  %d\n"
	"Memory clock rate:  %d\n"
	"Memory bus width:  %d\n"
	"Multiprocessor count:  %d\n"
	"Concurrent kernels:  %d\n"
        "Compute mode:  %d\n\n",
        device, dp.name, dp.major, dp.minor, dp.totalGlobalMem, dp.maxThreadsPerBlock, dp.warpSize, dp.clockRate,
            dp.memoryClockRate, dp.memoryBusWidth, dp.multiProcessorCount, dp.concurrentKernels, dp.computeMode);
  }

  return 0;
}

