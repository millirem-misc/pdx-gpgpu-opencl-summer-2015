#include <stdio.h>

int main(void) {
  int *version = new int;
  int deviceCount;
  int device;
  cudaDeviceProp dp;

  cudaGetDeviceCount(&deviceCount);
  for (device = 0; device < deviceCount; device++) {
    cudaGetDeviceProperties(&dp, device);
    printf("%s\n", dp.name);
  }

  return 0;
}

