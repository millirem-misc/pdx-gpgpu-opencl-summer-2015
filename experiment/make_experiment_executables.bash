#!/bin/bash


cd ../tile_convolution

#make type_int cpu
#mv convolution_cpu ../experiment/convolution-int-cpu
make type_int tile_none cuda
mv tile_convolution_cuda ../experiment/convolution-int-none-cuda
make type_int tile_naive cuda
mv tile_convolution_cuda ../experiment/convolution-int-naive-cuda
make type_int tile_branch cuda
mv tile_convolution_cuda ../experiment/convolution-int-branch-cuda
make type_int tile_threads cuda
mv tile_convolution_cuda ../experiment/convolution-int-threads-cuda
make type_int tile_none opencl
mv tile_convolution_cl ../experiment/convolution-int-none-opencl
make type_int tile_naive opencl
mv tile_convolution_cl ../experiment/convolution-int-naive-opencl
make type_int tile_branch opencl
mv tile_convolution_cl ../experiment/convolution-int-branch-opencl
make type_int tile_threads opencl
mv tile_convolution_cl ../experiment/convolution-int-threads-opencl
#make type_float cpu
#mv convolution_cpu ../experiment/convolution-float-cpu
make type_float tile_none cuda
mv tile_convolution_cuda ../experiment/convolution-float-none-cuda
make type_float tile_naive cuda
mv tile_convolution_cuda ../experiment/convolution-float-naive-cuda
make type_float tile_branch cuda
mv tile_convolution_cuda ../experiment/convolution-float-branch-cuda
make type_float tile_threads cuda
mv tile_convolution_cuda ../experiment/convolution-float-threads-cuda
make type_float tile_none opencl
mv tile_convolution_cl ../experiment/convolution-float-none-opencl
make type_float tile_naive  opencl
mv tile_convolution_cl ../experiment/convolution-float-naive-opencl
make type_float tile_branch opencl
mv tile_convolution_cl ../experiment/convolution-float-branch-opencl
make type_float tile_threads opencl
mv tile_convolution_cl ../experiment/convolution-float-threads-opencl

cd ../experiment
chmod ugo+x convolution*

