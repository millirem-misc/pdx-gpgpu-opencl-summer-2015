Host:  pacific
Width lower limit:  4
Width upper limit:  16384
Width stride:  2
Height lower limit:  4
Height upper limit:  16384
Height stride:  2
Tile width lower limit:  8
Tile width upper limit:  32
Tile width stride:  2
Mask width lower limit:  3
Mask width upper limit:  7
Mask width stride:  2
Repetitions:  3
Reduction type:  AVG
2015-08-09__20.49.59.791116
 20:49:59 up 19 days, 19:54,  1 user,  load average: 0.07, 0.03, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,cuda,simple2015-08-09__21.23.15.511137
 21:23:15 up 19 days, 20:28,  1 user,  load average: 1.12, 1.05, 0.92
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,cuda,branching2015-08-09__21.55.26.316354
 21:55:26 up 19 days, 21:00,  1 user,  load average: 1.02, 1.04, 0.98
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,cuda,threads2015-08-09__22.27.39.769387
 22:27:39 up 19 days, 21:32,  1 user,  load average: 1.00, 1.00, 1.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,opencl,simple2015-08-09__22.59.34.159887
 22:59:34 up 19 days, 22:04,  1 user,  load average: 1.00, 1.05, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,opencl,branching2015-08-09__23.31.29.970192
 23:31:30 up 19 days, 22:36,  1 user,  load average: 1.02, 1.02, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,opencl,threads2015-08-09__23.55.47.189535
 23:55:47 up 19 days, 23:00,  1 user,  load average: 1.00, 1.02, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,cuda,simple2015-08-10__00.37.16.819027
 00:37:16 up 19 days, 23:42,  1 user,  load average: 1.01, 1.02, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,cuda,branching2015-08-10__01.18.29.488830
 01:18:29 up 20 days, 23 min,  1 user,  load average: 1.00, 1.00, 1.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,cuda,threads2015-08-10__01.59.41.931888
 01:59:42 up 20 days,  1:04,  1 user,  load average: 0.99, 1.00, 1.03
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,opencl,simple2015-08-10__02.41.02.815689
 02:41:02 up 20 days,  1:45,  1 user,  load average: 1.00, 1.01, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,opencl,branching2015-08-10__03.22.22.671548
 03:22:22 up 20 days,  2:27,  1 user,  load average: 1.00, 1.01, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,opencl,threads2015-08-10__03.54.02.011558
 03:54:02 up 20 days,  2:58,  1 user,  load average: 1.01, 1.05, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
