Host:  pacific
Width lower limit:  4
Width upper limit:  16384
Width stride:  2
Height lower limit:  4
Height upper limit:  16384
Height stride:  2
Tile width lower limit:  8
Tile width upper limit:  32
Tile width stride:  2
Mask width lower limit:  3
Mask width upper limit:  7
Mask width stride:  2
Repetitions:  5
Reduction type:  AVG
2015-08-09__00.31.13.326829
 00:31:13 up 18 days, 23:36,  1 user,  load average: 0.03, 0.03, 0.12
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,cuda,simple2015-08-09__00.32.05.481441
 00:32:05 up 18 days, 23:36,  1 user,  load average: 0.88, 0.26, 0.20
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,cuda,branching2015-08-09__00.32.57.157091
 00:32:57 up 18 days, 23:37,  1 user,  load average: 0.99, 0.39, 0.24
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,cuda,threads2015-08-09__00.33.48.521381
 00:33:48 up 18 days, 23:38,  1 user,  load average: 1.07, 0.52, 0.30
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,opencl,simple2015-08-09__00.34.42.065532
 00:34:42 up 18 days, 23:39,  1 user,  load average: 1.08, 0.61, 0.34
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,opencl,branching2015-08-09__00.35.35.291601
 00:35:35 up 18 days, 23:40,  1 user,  load average: 1.03, 0.67, 0.38
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,integer,opencl,threads2015-08-09__00.36.28.661502
 00:36:28 up 18 days, 23:41,  1 user,  load average: 1.05, 0.73, 0.41
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,cuda,simple2015-08-09__00.37.20.758943
 00:37:20 up 18 days, 23:42,  1 user,  load average: 1.09, 0.79, 0.45
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,cuda,branching2015-08-09__00.38.12.712523
 00:38:12 up 18 days, 23:43,  1 user,  load average: 1.08, 0.85, 0.49
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,cuda,threads2015-08-09__00.39.04.713261
 00:39:04 up 18 days, 23:43,  1 user,  load average: 1.25, 0.93, 0.54
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,opencl,simple2015-08-09__00.39.58.171769
 00:39:58 up 18 days, 23:44,  1 user,  load average: 0.89, 0.90, 0.55
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,opencl,branching2015-08-09__00.40.51.991901
 00:40:51 up 18 days, 23:45,  1 user,  load average: 0.95, 0.91, 0.57
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
pacific,fp,opencl,threads2015-08-09__00.41.45.185537
 00:41:45 up 18 days, 23:46,  1 user,  load average: 0.96, 0.93, 0.60
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
