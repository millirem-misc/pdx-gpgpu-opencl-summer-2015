SELECT Width, DataType, GPULanguage, COUNT(MinKernelTimePerUnitArea) FROM
  (SELECT *, printf("%.3f", MIN(KernelTimePerUnitArea_Sub)) AS MinKernelTimePerUnitArea FROM
    (SELECT *, TIMING / (CAST(Width * Height AS FLOAT)) AS KernelTimePerUnitArea_Sub from original_data
    WHERE TimedBlockID = 'kernel')
  GROUP BY Width, Host, Algorithm, TileWidth, MaskWidth)
GROUP BY Width, DataType, GPULanguage
ORDER BY Width, DataType
