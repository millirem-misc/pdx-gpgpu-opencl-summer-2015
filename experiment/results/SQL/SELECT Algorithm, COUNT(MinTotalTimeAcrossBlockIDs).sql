SELECT Algorithm, COUNT(MinTotalTimeAcrossBlockIDs) FROM
  (SELECT Algorithm, MIN(TotalTimeAcrossBlockIDs) AS MinTotalTimeAcrossBlockIDs FROM
    (SELECT Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth, SUM(TIMING) AS TotalTimeAcrossBlockIDs from original_data
     GROUP BY Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth)
  GROUP BY Host, DataType, GPULanguage, Width, Height, TileWidth, MaskWidth)
GROUP BY Algorithm