SELECT Host, COUNT(MinTimePerUnitArea) FROM
  (SELECT Host, printf("%.3f", MIN(TimePerUnitArea_Sub)) AS MinTimePerUnitArea FROM
    (SELECT *, TIMING / (CAST(Width * Height AS FLOAT)) AS TimePerUnitArea_Sub from original_data)
  GROUP BY DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth, TimedBlockID)
GROUP BY Host