SELECT GPULanguage, COUNT(MinTimePerUnitArea) FROM
  (SELECT GPULanguage, printf("%.3f", MIN(TimePerUnitArea_Sub)) AS MinTimePerUnitArea FROM
    (SELECT *, TIMING / (CAST(Width * Height AS FLOAT)) AS TimePerUnitArea_Sub from original_data)
  GROUP BY Host, DataType, Algorithm, Width, Height, TileWidth, MaskWidth, TimedBlockID)
GROUP BY GPULanguage