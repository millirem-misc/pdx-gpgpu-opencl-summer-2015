SELECT Width, GPULanguage, Algorithm, AVG(TotalTimePerUnitArea) AS AvgTotalTimePerUnitArea FROM (
  SELECT Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth, SUM(TIMING) AS TotalTimeAcrossBlockIDs, SUM(TIMING) / (CAST(Width * Height AS FLOAT)) AS 
   TotalTimePerUnitArea FROM original_data
  GROUP BY Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth
) GROUP BY GPULanguage, Algorithm, Width
ORDER BY Width, GPULanguage, Algorithm