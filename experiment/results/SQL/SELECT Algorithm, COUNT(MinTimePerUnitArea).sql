SELECT Algorithm, COUNT(MinTimePerUnitArea) FROM
  (SELECT Algorithm, printf("%.3f", MIN(TimePerUnitArea_Sub)) AS MinTimePerUnitArea FROM
    (SELECT *, TIMING / (CAST(Width * Height AS FLOAT)) AS TimePerUnitArea_Sub from original_data)
  GROUP BY Host, DataType, GPULanguage, Width, Height, TileWidth, MaskWidth, TimedBlockID)
GROUP BY Algorithm