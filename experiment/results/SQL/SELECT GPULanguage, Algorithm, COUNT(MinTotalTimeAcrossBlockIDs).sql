SELECT GPULanguage, Algorithm, COUNT(MinTotalTimeAcrossBlockIDs) FROM (
  SELECT GPULanguage, Algorithm, MIN(TotalTimeAcrossBlockIDs) AS MinTotalTimeAcrossBlockIDs FROM (
    SELECT Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth, SUM(TIMING) AS TotalTimeAcrossBlockIDs from original_data
    GROUP BY Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth
  ) GROUP BY Host, DataType, Width, Height, TileWidth, MaskWidth
) GROUP BY GPULanguage, Algorithm