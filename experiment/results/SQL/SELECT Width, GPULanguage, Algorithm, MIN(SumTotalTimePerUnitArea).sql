SELECT Width, GPULanguage, Algorithm, MIN(SumTotalTimePerUnitArea) FROM (
  SELECT GPULanguage, Algorithm, Width, SUM(TotalTimeAcrossBlockIDs) AS SumTotalTimeAcrossBlockIDs, SUM(TotalTimeAcrossBlockIDs) / (CAST(Width * Height AS FLOAT)) AS 
   SumTotalTimePerUnitArea FROM (
    SELECT Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth, SUM(TIMING) AS TotalTimeAcrossBlockIDs, SUM(TIMING) / (CAST(Width * Height AS FLOAT)) AS 
     TotalTimePerUnitArea FROM original_data
    GROUP BY Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth
  ) GROUP BY GPULanguage, Algorithm, Width
) GROUP BY Width