SELECT Host, DataType, GPULanguage, Algorithm, Width, Height, TileWidth, MaskWidth, TimedBlockID, printf("%.3f", TIMING) AS TIMING, printf("%.3f", TimePerUnitArea_Sub) AS TimePerUnitArea FROM
  (SELECT *, TIMING / (CAST(Width * Height AS FLOAT)) AS TimePerUnitArea_Sub from original_data)
ORDER BY Host, DataType, GPULanguage, Width, Height, TileWidth, MaskWidth, TimedBlockID, TimePerUnitArea_Sub
