SELECT GPULanguage, COUNT(MinKernelTimePerUnitArea) FROM
  (SELECT GPULanguage, printf("%.3f", MIN(KernelTimePerUnitArea_Sub)) AS MinKernelTimePerUnitArea FROM
    (SELECT *, TIMING / (CAST(Width * Height AS FLOAT)) AS KernelTimePerUnitArea_Sub from original_data)
   WHERE TimedBlockID = 'kernel'
   GROUP BY Host, DataType, Algorithm, Width, Height, TileWidth, MaskWidth, TimedBlockID)
GROUP BY GPULanguage