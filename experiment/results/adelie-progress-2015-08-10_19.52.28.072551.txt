Host:  adelie
Width lower limit:  4
Width upper limit:  8192
Width stride:  2
Height lower limit:  4
Height upper limit:  8192
Height stride:  2
Tile width lower limit:  8
Tile width upper limit:  32
Tile width stride:  2
Mask width lower limit:  3
Mask width upper limit:  7
Mask width stride:  2
Repetitions:  3
Reduction type:  AVG
2015-08-10__19.52.28.092727
 19:52:28 up 25 days,  5:22,  6 users,  load average: 0.00, 0.28, 0.52
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,integer,cuda,simple2015-08-10__20.12.01.217590
 20:12:01 up 25 days,  5:41,  6 users,  load average: 1.02, 1.00, 0.87
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,integer,cuda,branching2015-08-10__20.31.42.773986
 20:31:42 up 25 days,  6:01,  6 users,  load average: 1.03, 1.01, 0.97
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,integer,cuda,threads2015-08-10__20.51.17.004537
 20:51:17 up 25 days,  6:20,  6 users,  load average: 1.08, 1.02, 0.97
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,integer,opencl,simple2015-08-10__21.10.41.902908
 21:10:42 up 25 days,  6:40,  6 users,  load average: 1.31, 1.10, 1.02
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,integer,opencl,branching2015-08-10__21.30.02.706326
 21:30:02 up 25 days,  6:59,  6 users,  load average: 1.00, 1.01, 1.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,integer,opencl,threads2015-08-10__21.49.38.701431
 21:49:38 up 25 days,  7:19,  6 users,  load average: 0.99, 0.99, 1.03
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,fp,cuda,simple2015-08-10__22.14.09.292682
 22:14:09 up 25 days,  7:43,  6 users,  load average: 1.06, 1.03, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,fp,cuda,branching2015-08-10__22.38.14.047536
 22:38:14 up 25 days,  8:07,  6 users,  load average: 0.95, 1.01, 1.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,fp,cuda,threads2015-08-10__23.02.36.925422
 23:02:37 up 25 days,  8:32,  6 users,  load average: 1.03, 1.06, 1.07
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,fp,opencl,simple2015-08-10__23.26.47.136494
 23:26:47 up 25 days,  8:56,  6 users,  load average: 1.01, 1.04, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,fp,opencl,branching2015-08-10__23.50.55.748745
 23:50:55 up 25 days,  9:20,  6 users,  load average: 1.01, 1.02, 1.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
adelie,fp,opencl,threads2015-08-11__00.15.25.388317
 00:15:25 up 25 days,  9:44,  6 users,  load average: 1.02, 1.01, 1.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
