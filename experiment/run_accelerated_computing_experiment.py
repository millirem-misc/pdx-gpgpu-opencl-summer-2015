#!/usr/bin/python
#
# CS510 - Accelerated Computing, Summer 2015
# Project group C1:  comparative performance study of CUDA and OpenCL tiled convolution.
#
# This is the master script for generation of experimental timing data for comparison of
#   convolution executed on CPU, on GPU in CUDA, on GPU in OpenCL, tiled and not, etc.
#
# Authors:
#   Brian Hoff
#   Byron Marohn
#   Jim Miller
#   Yasodhadevi Nachimuthu
#
# Usage:  Edit appropriate global constants, then simply 'run_accelerated_computing_experiment.py'.


import collections
import datetime
import subprocess


DataType = collections.namedtuple("type", ["integer", "fp"])
GPULanguage = collections.namedtuple("language", ["cuda", "opencl"])
Algorithm = collections.namedtuple("algorithm", ["simple", "branching", "threads"])


# GLOBAL CONSTANTS
RUNTIME = str(datetime.datetime.now().date()) + "_" + str(datetime.datetime.now().time()).replace(":", ".")
HOSTNAME = subprocess.check_output(["uname", "-n"]).strip()
#CPUFREQ = "lscpu | grep MHz | sed 's/^CPU MHz: *//;s/\.//'"
#SENSORS = "sensors | grep -v coretemp | grep -v Adapter"

EXEC_FILE_PATH = subprocess.check_output(["pwd"]).strip()
EXEC_INT_CPU = EXEC_FILE_PATH + "/make_random_output.py"
#EXEC_INT_CPU = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_CUDA_SIMPLE = EXEC_FILE_PATH + "/convolution-int-none-cuda-BENCHMARK"
#EXEC_INT_CUDA_SIMPLE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_CUDA_NAIVE = EXEC_FILE_PATH + "/convolution-int-naive-cuda-BENCHMARK"
#EXEC_INT_CUDA_NAIVE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_CUDA_BRANCHING = EXEC_FILE_PATH + "/convolution-int-branch-cuda-BENCHMARK"
#EXEC_INT_CUDA_BRANCHING = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_CUDA_THREADS = EXEC_FILE_PATH + "/convolution-int-threads-cuda-BENCHMARK"
#EXEC_INT_CUDA_THREADS = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_OPENCL_SIMPLE = EXEC_FILE_PATH + "/convolution-int-none-opencl-BENCHMARK"
#EXEC_INT_OPENCL_SIMPLE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_OPENCL_NAIVE = EXEC_FILE_PATH + "/convolution-int-naive-opencl-BENCHMARK"
#EXEC_INT_OPENCL_NAIVE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_OPENCL_BRANCHING = EXEC_FILE_PATH + "/convolution-int-branch-opencl-BENCHMARK"
#EXEC_INT_OPENCL_BRANCHING = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_INT_OPENCL_THREADS = EXEC_FILE_PATH + "/convolution-int-threads-opencl-BENCHMARK"
#EXEC_INT_OPENCL_THREADS = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_CPU = EXEC_FILE_PATH + "/make_random_output.py"
#EXEC_FLOAT_CPU = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_CUDA_SIMPLE = EXEC_FILE_PATH + "/convolution-float-none-cuda-BENCHMARK"
#EXEC_FLOAT_CUDA_SIMPLE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_CUDA_NAIVE = EXEC_FILE_PATH + "/convolution-float-naive-cuda-BENCHMARK"
#EXEC_FLOAT_CUDA_NAIVE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_CUDA_BRANCHING = EXEC_FILE_PATH + "/convolution-float-branch-cuda-BENCHMARK"
#EXEC_FLOAT_CUDA_BRANCHING = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_CUDA_THREADS = EXEC_FILE_PATH + "/convolution-float-threads-cuda-BENCHMARK"
#EXEC_FLOAT_CUDA_THREADS = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_OPENCL_SIMPLE = EXEC_FILE_PATH + "/convolution-float-none-opencl-BENCHMARK"
#EXEC_FLOAT_OPENCL_SIMPLE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_OPENCL_NAIVE = EXEC_FILE_PATH + "/convolution-float-naive-opencl-BENCHMARK"
#EXEC_FLOAT_OPENCL_NAIVE = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_OPENCL_BRANCHING = EXEC_FILE_PATH + "/convolution-float-branch-opencl-BENCHMARK"
#EXEC_FLOAT_OPENCL_BRANCHING = EXEC_FILE_PATH + "/make_random_output.py"
EXEC_FLOAT_OPENCL_THREADS = EXEC_FILE_PATH + "/convolution-float-threads-opencl-BENCHMARK"
#EXEC_FLOAT_OPENCL_THREADS = EXEC_FILE_PATH + "/make_random_output.py"

WIDTH_ARG_LOWERLIMIT = 4
WIDTH_ARG_UPPERLIMIT = 8192
# Multiplicative stride
WIDTH_ARG_STRIDE = 2

HEIGHT_ARG_LOWERLIMIT = 4
HEIGHT_ARG_UPPERLIMIT = 8192
# Multiplicative stride
HEIGHT_ARG_STRIDE = 2

TILE_WIDTH_ARG_LOWERLIMIT = 8
TILE_WIDTH_ARG_UPPERLIMIT = 32
# Multiplicative stride
TILE_WIDTH_ARG_STRIDE = 2

MASK_WIDTH_ARG_LOWERLIMIT = 3
MASK_WIDTH_ARG_UPPERLIMIT = 7
# Additive stride
MASK_WIDTH_ARG_STRIDE = 2

DATA_FILE_PATH = subprocess.check_output(["pwd"]).strip()
MATRIX_INT_FILE_NAME = "matrix-int-data.txt"
MATRIX_INT_FILE = DATA_FILE_PATH + "/" + MATRIX_INT_FILE_NAME
MATRIX_FLOAT_FILE_NAME = "matrix-float-data.txt"
MATRIX_FLOAT_FILE = DATA_FILE_PATH + "/" + MATRIX_FLOAT_FILE_NAME
MASK_INT_FILE_NAME = "mask-int-data.txt"
MASK_INT_FILE = DATA_FILE_PATH + "/" + MASK_INT_FILE_NAME
MASK_FLOAT_FILE_NAME = "mask-float-data.txt"
MASK_FLOAT_FILE = DATA_FILE_PATH + "/" + MASK_FLOAT_FILE_NAME
SAVE_FILE_NAME = HOSTNAME + "-timing-data-" + RUNTIME + ".txt"
SAVE_FILE = DATA_FILE_PATH + "/" + SAVE_FILE_NAME
PROGRESS_FILE_NAME = HOSTNAME + "-progress-" + RUNTIME + ".txt"
PROGRESS_FILE = DATA_FILE_PATH + "/" + PROGRESS_FILE_NAME
ERROR_FILE_NAME = HOSTNAME + "-error-" + RUNTIME + ".txt"
ERROR_FILE = DATA_FILE_PATH + "/" + ERROR_FILE_NAME

DELIMITER = ","
REPETITIONS = 3
TIMING_VALUE_NOT_APPLICABLE = -1

# Choices are "AVG", "MIN", or "MAX"
REDUCTION_TYPE = "AVG"


# GLOBAL VARS
currentDataType = ""
currentGPULanguage = ""
currentAlgorithm = ""
currentWidth = 0
currentHeight = 0
currentTileWidth = 0
currentMaskWidth = 0
currentTimedBlockID = ""
currentExperiment = ""
currentArgs = ""

timedBlockIDs = []
currentResults = []
setupTimings = []
mallocTimings = []
memcpyTimings = []
kernelTimings = []
reducedTimings = []

saveFile = open(SAVE_FILE, 'w')
progressFile = open(PROGRESS_FILE, 'w')
errorFile = open(ERROR_FILE, 'w')
devnullFile = open("/dev/null", 'w')

measurementNumber = 0

computationError = False


# Routine for gathering timing data for a single set of parameters
def runSingleExperiment():
  global measurementNumber
  global timedBlockIDs
  global reducedTimings
  global currentResults
  global setupTimings
  global mallocTimings
  global memcpyTimings
  global kernelTimings
  global computationError

  currentResultsTotal = ""

  timedBlockIDs = []
  reducedTimings = []
  currentArgsList = []
  currentFullArgsList = []

  currentFullArgsList.append(currentExperiment)
  currentArgsList = currentArgs.split()
  currentArgsList = [str(j) for j in currentArgsList]
  for j in currentArgsList:
    currentFullArgsList.append(j)
  for i in xrange(0, REPETITIONS):
    try:
      currentResultsTotal = subprocess.check_output(currentFullArgsList, stderr=errorFile)
    except subprocess.CalledProcessError, e:
      computationError = True
      errorFile.write(HOSTNAME + DELIMITER + currentDataType + DELIMITER + ("none" if currentAlgorithm == "cpu" else currentGPULanguage) + \
       DELIMITER + currentAlgorithm + DELIMITER + str(currentWidth) + DELIMITER + str(currentHeight) + DELIMITER + \
       str(currentTileWidth) + DELIMITER + str(currentMaskWidth) + "\n")
      errorFile.write(e.output)
      return
    currentResults = currentResultsTotal.split('\n')
    blocks =[None] * 4
    timings = [None] * 4
    for j in xrange(0, 4):
      blocks[j], timings[j] = currentResults[j].split()
      # Throw out first time measurement because of possible influence of non-cached data in the first run
    if i == 0:
      for j in xrange(0, 4):
        timedBlockIDs.append(blocks[j])
    else:
      setupTimings.append(float(timings[0]))
      mallocTimings.append(float(timings[1]))
      memcpyTimings.append(float(timings[2]))
      kernelTimings.append(float(timings[3]))

  if REDUCTION_TYPE == "AVG":
    reducedTimings.append((sum(setupTimings)) / float(len(setupTimings)))
    reducedTimings.append((sum(mallocTimings)) / float(len(mallocTimings)))
    reducedTimings.append((sum(memcpyTimings)) / float(len(memcpyTimings)))
    reducedTimings.append((sum(kernelTimings)) / float(len(kernelTimings)))
  else:
    if REDUCTION_TYPE == "MIN":
      reducedTimings.append(min(setupTimings))
      reducedTimings.append(min(mallocTimings))
      reducedTimings.append(min(memcpyTimings))
      reducedTimings.append(min(kernelTimings))
    else:
      if REDUCTION_TYPE == "MAX":
        reducedTimings.append(max(setupTimings))
        reducedTimings.append(max(mallocTimings))
        reducedTimings.append(max(memcpyTimings))
        reducedTimings.append(max(kernelTimings))

  currentResults = []
  setupTimings = []
  mallocTimings = []
  memcpyTimings = []
  kernelTimings = []

  measurementNumber += REPETITIONS


# MAIN ROUTINE
saveFile.write("Host" + DELIMITER + "DataType" + DELIMITER + "GPULanguage" + DELIMITER + "Algorithm" + DELIMITER + "Width" + \
 DELIMITER + "Height" + DELIMITER + "TileWidth" + DELIMITER + "MaskWidth" + DELIMITER + "TimedBlockID" + DELIMITER + "TIMING\n")
progressFile.write("Host:  " + HOSTNAME + "\nWidth lower limit:  " + str(WIDTH_ARG_LOWERLIMIT) + "\nWidth upper limit:  " + \
 str(WIDTH_ARG_UPPERLIMIT) + "\nWidth stride:  " + str(WIDTH_ARG_STRIDE) + "\nHeight lower limit:  " + str(HEIGHT_ARG_LOWERLIMIT) + \
 "\nHeight upper limit:  " + str(HEIGHT_ARG_UPPERLIMIT) + "\nHeight stride:  " + str(HEIGHT_ARG_STRIDE) + \
 "\nTile width lower limit:  " + str(TILE_WIDTH_ARG_LOWERLIMIT) + "\nTile width upper limit:  " + str(TILE_WIDTH_ARG_UPPERLIMIT) + \
 "\nTile width stride:  " + str(TILE_WIDTH_ARG_STRIDE) + "\nMask width lower limit:  " + str(MASK_WIDTH_ARG_LOWERLIMIT) + \
 "\nMask width upper limit:  " + str(MASK_WIDTH_ARG_UPPERLIMIT) + "\nMask width stride:  " + str(MASK_WIDTH_ARG_STRIDE) + \
 "\nRepetitions:  " + str(REPETITIONS) + "\nReduction type:  " + REDUCTION_TYPE + "\n")
print str(datetime.datetime.now().date()) + "__" + str(datetime.datetime.now().time()).replace(":", ".")
progressFile.write(str(datetime.datetime.now().date()) + "__" + str(datetime.datetime.now().time()).replace(":", ".") + "\n")
progressFile.write(subprocess.check_output(["w", ""]))

for currentDataType in DataType._fields:
  for currentGPULanguage in GPULanguage._fields:
    for currentAlgorithm in Algorithm._fields:
      if currentGPULanguage == "opencl" and currentAlgorithm == "cpu":
        continue
      print HOSTNAME + DELIMITER + currentDataType + DELIMITER + ("none" if currentAlgorithm == "cpu" else currentGPULanguage) + \
       DELIMITER + currentAlgorithm
      # Ugly ugly I know
      if currentDataType == "integer":
        if currentGPULanguage == "cuda":
          if currentAlgorithm == "simple":
            currentExperiment = EXEC_INT_CUDA_SIMPLE
          else:
            if currentAlgorithm == "naive":
              currentExperiment = EXEC_INT_CUDA_NAIVE
            else:
              if currentAlgorithm == "branching":
                currentExperiment = EXEC_INT_CUDA_BRANCHING
              else:
                if currentAlgorithm == "threads":
                  currentExperiment = EXEC_INT_CUDA_THREADS
                else:
                  currentExperiment = EXEC_INT_CPU
        else:
          if currentAlgorithm == "simple":
            currentExperiment = EXEC_INT_OPENCL_SIMPLE
          else:
            if currentAlgorithm == "naive":
              currentExperiment = EXEC_INT_OPENCL_NAIVE
            else:
              if currentAlgorithm == "branching":
                currentExperiment = EXEC_INT_OPENCL_BRANCHING
              else:
                currentExperiment = EXEC_INT_OPENCL_THREADS
      else:
        if currentGPULanguage == "cuda":
          if currentAlgorithm == "simple":
            currentExperiment = EXEC_FLOAT_CUDA_SIMPLE
          else:
            if currentAlgorithm == "naive":
              currentExperiment = EXEC_FLOAT_CUDA_NAIVE
            else:
              if currentAlgorithm == "branching":
                currentExperiment = EXEC_FLOAT_CUDA_BRANCHING
              else:
                if currentAlgorithm == "threads":
                  currentExperiment = EXEC_FLOAT_CUDA_THREADS
                else:
                  currentExperiment = EXEC_FLOAT_CPU
        else:
          if currentAlgorithm == "simple":
            currentExperiment = EXEC_FLOAT_OPENCL_SIMPLE
          else:
            if currentAlgorithm == "naive":
              currentExperiment = EXEC_FLOAT_OPENCL_NAIVE
            else:
              if currentAlgorithm == "branching":
                currentExperiment = EXEC_FLOAT_OPENCL_BRANCHING
              else:
                currentExperiment = EXEC_FLOAT_OPENCL_THREADS

      currentWidth = WIDTH_ARG_LOWERLIMIT
      while currentWidth <= WIDTH_ARG_UPPERLIMIT:
        currentHeight = HEIGHT_ARG_LOWERLIMIT
        while currentHeight <= HEIGHT_ARG_UPPERLIMIT:
          if currentHeight != currentWidth:
            currentHeight *= HEIGHT_ARG_STRIDE
            continue
          currentTileWidth = TILE_WIDTH_ARG_LOWERLIMIT
          while currentTileWidth <= TILE_WIDTH_ARG_UPPERLIMIT:
            if currentTileWidth > currentWidth or currentTileWidth > currentHeight:
              break
            currentMaskWidth = MASK_WIDTH_ARG_LOWERLIMIT
            while currentMaskWidth <= MASK_WIDTH_ARG_UPPERLIMIT:
              if currentMaskWidth >= currentTileWidth or currentMaskWidth >= currentWidth or currentMaskWidth >= currentHeight:
                break

              if currentDataType == "integer":
                currentArgs = "--input-matrix " + MATRIX_INT_FILE + " --width " + str(currentWidth) + " --height " + str(currentHeight) + \
                 " --input-mask " + MASK_INT_FILE + " --mask-width " + str(currentMaskWidth) + " --tile-width " + str(currentTileWidth)
              else:
                currentArgs = "--input-matrix " + MATRIX_FLOAT_FILE + " --width " + str(currentWidth) + " --height " + str(currentHeight) + \
                 " --input-mask " + MASK_FLOAT_FILE + " --mask-width " + str(currentMaskWidth) + " --tile-width " + str(currentTileWidth)

              runSingleExperiment()

              if computationError == False:
                for i in xrange(0, len(timedBlockIDs)):
                  saveFile.write(HOSTNAME + DELIMITER + currentDataType + DELIMITER + ("none" if currentAlgorithm == "cpu" else currentGPULanguage) + \
                   DELIMITER + currentAlgorithm + DELIMITER + str(currentWidth) + DELIMITER + str(currentHeight) + DELIMITER + \
                   str(currentTileWidth) + DELIMITER + str(currentMaskWidth) + DELIMITER + timedBlockIDs[i] + DELIMITER + \
                   "%.3f\n" % reducedTimings[i])
              else:
                computationError = False

              currentMaskWidth += MASK_WIDTH_ARG_STRIDE
            currentTileWidth *= TILE_WIDTH_ARG_STRIDE
          currentHeight *= HEIGHT_ARG_STRIDE
        currentWidth *= WIDTH_ARG_STRIDE
      print "Successful measurement #:  "  + str(measurementNumber)
      print str(datetime.datetime.now().date()) + "__" + str(datetime.datetime.now().time()).replace(":", ".")
      progressFile.write(HOSTNAME + DELIMITER + currentDataType + DELIMITER + ("none" if currentAlgorithm == "cpu" else currentGPULanguage) + \
       DELIMITER + currentAlgorithm)
      progressFile.write(str(datetime.datetime.now().date()) + "__" + str(datetime.datetime.now().time()).replace(":", ".") + "\n")
      progressFile.write(subprocess.check_output(["w", ""]))

saveFile.close()
progressFile.close()
errorFile.close()
devnullFile.close()

