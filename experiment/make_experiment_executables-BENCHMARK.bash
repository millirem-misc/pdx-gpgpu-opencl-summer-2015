#!/bin/bash


cd ../tile_convolution

make type_int tile_none benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-int-none-cuda-BENCHMARK
make type_int tile_naive benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-int-naive-cuda-BENCHMARK
make type_int tile_branch benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-int-branch-cuda-BENCHMARK
make type_int tile_threads benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-int-threads-cuda-BENCHMARK
make type_int tile_none benchmark opencl
mv tile_convolution_cl ../experiment/convolution-int-none-opencl-BENCHMARK
make type_int tile_naive benchmark opencl
mv tile_convolution_cl ../experiment/convolution-int-naive-opencl-BENCHMARK
make type_int tile_branch benchmark opencl
mv tile_convolution_cl ../experiment/convolution-int-branch-opencl-BENCHMARK
make type_int tile_threads benchmark opencl
mv tile_convolution_cl ../experiment/convolution-int-threads-opencl-BENCHMARK
make type_float tile_none benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-float-none-cuda-BENCHMARK
make type_float tile_naive benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-float-naive-cuda-BENCHMARK
make type_float tile_branch benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-float-branch-cuda-BENCHMARK
make type_float tile_threads benchmark cuda
mv tile_convolution_cuda ../experiment/convolution-float-threads-cuda-BENCHMARK
make type_float tile_none benchmark opencl
mv tile_convolution_cl ../experiment/convolution-float-none-opencl-BENCHMARK
make type_float tile_naive benchmark opencl
mv tile_convolution_cl ../experiment/convolution-float-naive-opencl-BENCHMARK
make type_float tile_branch benchmark opencl
mv tile_convolution_cl ../experiment/convolution-float-branch-opencl-BENCHMARK
make type_float tile_threads benchmark opencl
mv tile_convolution_cl ../experiment/convolution-float-threads-opencl-BENCHMARK

cd ../experiment
chmod ugo+x convolution*

