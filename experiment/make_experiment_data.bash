#!/bin/bash


./convolution-int-none-cuda-BENCHMARK --gen-input matrix-int-data.txt --width 16384 --height 16384
./convolution-float-none-cuda-BENCHMARK --gen-input matrix-float-data.txt --width 16384 --height 16384
./convolution-int-none-cuda-BENCHMARK --gen-mask mask-int-data.txt --mask-width 25
./convolution-float-none-cuda-BENCHMARK --gen-mask mask-float-data.txt --mask-width 25

