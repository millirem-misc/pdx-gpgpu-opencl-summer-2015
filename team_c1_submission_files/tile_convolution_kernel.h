#if defined(CONVO_TYPE_INT)
typedef int DATA_TYPE;
#elif defined (CONVO_TYPE_FLOAT)
typedef float DATA_TYPE;
#endif

#if defined(CONVO_CUDA)
__global__ void convolute_kernel(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix,
                                 const DATA_TYPE *mask_matrix, int width, int height, int mask_width)
#elif defined(CONVO_OPENCL)
__kernel void convolute_kernel(__global DATA_TYPE *out_matrix, __global DATA_TYPE *in_matrix,
                               __global const DATA_TYPE *mask_matrix, int width, int height,
                               int mask_width, local DATA_TYPE N_ds[])
#endif
{
	int x, y;
	int local_x, local_y;
	int local_dim_x, local_dim_y;
	int local_blk_x, local_blk_y;
	int out_index;
	int in_x_start, in_y_start;
	DATA_TYPE out_value = 0;

#if defined(CONVO_CUDA)
	x = blockIdx.x * blockDim.x + threadIdx.x;
	y = blockIdx.y * blockDim.y + threadIdx.y;
	local_x = threadIdx.x;
	local_y = threadIdx.y;
	local_dim_x = blockDim.x;
	local_dim_y = blockDim.y;
	local_blk_x = blockIdx.x;
	local_blk_y = blockIdx.y;
	extern __shared__ DATA_TYPE N_ds[];
#elif defined(CONVO_OPENCL)
	x = get_global_id(0);
	y = get_global_id(1);
	local_x = get_local_id(0);
	local_y = get_local_id(1);
	local_dim_x = get_local_size(0);
	local_dim_y = get_local_size(1);
	local_blk_x = get_group_id(0);
	local_blk_y = get_group_id(1);
	// TODO: Remove this when there are no more printfs
#define printf(...)
#endif

////////////////////////////////////////////////
// Simple convolution with no shared memory
////////////////////////////////////////////////
#if defined(SIMPLE_CONVOLUTION)
	if (x < width && y < height) {
		out_index = (y * width) + x;
		in_x_start = x - mask_width / 2;
		in_y_start = y - mask_width / 2;
		out_value = 0;
		//printf("x start is %d , y start is %d \n",in_x_start, in_y_start);
		for (int j = 0; j < mask_width; j++) {
			for (int k = 0; k < mask_width; k++) {
				if (in_x_start + k >= 0 && in_y_start + j >= 0 && in_x_start + k < width && in_y_start + j < height)
					out_value += in_matrix[((in_y_start + j) * width) + in_x_start + k]
					             * mask_matrix[j * mask_width + k];
			}
		}
		out_matrix[out_index] = out_value;
	}
#elif defined(TILED_CONVOLUTION) // was assumed default
	if (x < width && y < height) {
		int n = mask_width / 2;
		int n_ds_y_mult = local_dim_x + mask_width - 1;
		in_x_start = x - n;
		in_y_start = y - n;
		out_index = (y * width) + x;
		out_value = 0;
		// hack for last blocks in x & y
		if ((((width / local_dim_x) == local_blk_x) && (width % local_dim_x != 0))
		    || (((height / local_dim_y) == local_blk_y) && (height % local_dim_y != 0))) {
			//printf(" In last x is %d y is %d blockIdx.x is %d blockIdx.y is %d   \n",local_x, local_y,blockIdx.x,blockIdx.y);
			for (int j = 0; j < mask_width; j++) {
				for (int k = 0; k < mask_width; k++) {
					if (in_x_start + k >= 0 && in_y_start + j >= 0 && in_x_start + k < width && in_y_start + j < height)
						out_value += in_matrix[((in_y_start + j) * width) + in_x_start + k]
						             * mask_matrix[j * mask_width + k];

				}
			}
		} else {
			// Loading bottom halo
			if (local_y < n) {
				N_ds[(local_y * n_ds_y_mult) + local_x + n] = in_y_start < 0
				                ? 0 : in_matrix[(in_y_start * width) + x];
				//printf(" In bottom x is %d y is %d blockIdx.x is %d blockIdx.y is %d  N_ds[%d][%d] is %d \n",local_x, local_y,blockIdx.x,blockIdx.y,local_y, local_x+n, N_ds[local_y][local_x+n]);

			}
			// Loading top halo
			else if (local_y >= local_dim_y - n) {
				N_ds[((local_y + n + n)*n_ds_y_mult) + local_x + n] = (y + n) >= height
				                ? 0 : in_matrix[((y + n) * width) + x];
			}
			// Loading left halo
			if (local_x < n) {
				N_ds[((local_y + n)*n_ds_y_mult) + local_x] = in_x_start < 0
				                ? 0 : in_matrix[(y * width) + in_x_start];
			}
			//Loading right halo
			else if (local_x >= local_dim_x - n) {
				N_ds[((local_y + n)*n_ds_y_mult) + local_x + n + n] = (x + n) >= width
				                ? 0 : in_matrix[(y * width) + x + n];
			}
			// Load left lower corner
			if (local_x == 0 && local_y == 0) {
				for (int my = 0; my < n; my++) {
					for (int mx = 0; mx < n; mx++) {
						N_ds[(my * n_ds_y_mult) + mx] = (in_x_start < 0 || in_y_start < 0)
						                                ? 0 : in_matrix[((in_y_start + my) * width) + in_x_start + mx];
					}
				}
			}
			// Load left upper corner
			else if (local_x == 0 && local_y == local_dim_y - 1) {
				int y_pos = y + 1;
				for (int my = local_y + n + 1; my < (local_y + n + n + 1); my++) {
					for (int mx = 0; mx < n; mx++) {
						N_ds[(my * n_ds_y_mult) + mx] = (in_x_start < 0 || y_pos >= height)
						                                ? 0 : in_matrix[((y_pos) * width) + in_x_start + mx];
					}
					y_pos++;
				}
			}
			// Load right lower corner
			else if (local_y == 0 && local_x == local_dim_x - 1) {
				for (int my = 0; my < n; my++) {
					int x_pos = x + 1;
					for (int mx = local_x + n + 1; mx < local_x + n + n + 1; mx++) {
						N_ds[(my * n_ds_y_mult) + mx] = (x_pos >= width || in_y_start < 0)
						                                ? 0 : in_matrix[((in_y_start + my) * width) + x_pos];
						x_pos++;
					}
				}
			}
			// Load right upper corner
			else if ((local_y == (local_dim_y - 1)) && ((local_x == (local_dim_x - 1)))) {
				int y_pos = y + 1;
				for (int my = local_y + n + 1; my < local_y + n + n + 1; my++) {
					int x_pos = x + 1;
					for (int mx = local_x + n + 1; mx < local_x + n + n + 1; mx++) {
						N_ds[(my * n_ds_y_mult) + mx] = (x_pos >= width || y_pos >= height)
						                                ? 0 : in_matrix[((y_pos) * width) + (x_pos)];
						x_pos++;
					}
					y_pos++;
				}
			}
			// Load each threads element
			N_ds[((local_y + n)*n_ds_y_mult) + local_x + n] = in_matrix[(y * width) + x];
#if defined(CONVO_CUDA)
			__syncthreads();
#elif defined(CONVO_OPENCL)
			barrier(CLK_LOCAL_MEM_FENCE);
#endif
			for (int j = 0; j < mask_width; j++) {
				for (int k = 0; k < mask_width; k++) {
					out_value += N_ds[((local_y + j) * n_ds_y_mult) + local_x + k] * mask_matrix[j * mask_width + k];
				}
			}
		}
		out_matrix[out_index] = out_value;
	}

#elif defined(CONVO_TILE_THREADS)

	uint offset = local_dim_x - mask_width + 1;
	uint edge = mask_width / 2;

	int gx = local_x + offset * local_blk_x - edge;
	int gy = local_y + offset * local_blk_y - edge;

	bool isZero = (gx < 0 ||
	               gx >= width ||
	               gy < 0 ||
	               gy >= height);

	uint local_idx = local_x + local_y * local_dim_x;
	uint gidx = gx + gy * width;

	N_ds[local_idx] = isZero ? 0 : in_matrix[gidx];

#if defined(CONVO_CUDA)
	__syncthreads();
#elif defined(CONVO_OPENCL)
	barrier(CLK_LOCAL_MEM_FENCE);
#endif

	if (!isZero && local_x >= edge && local_y >= edge && local_x < local_dim_x - edge
	    && local_y < local_dim_y - edge) {
		out_index = gidx;

		for (uint i = 0; i < mask_width; ++i) {
			for (uint j = 0; j < mask_width; ++j) {
				out_value += N_ds[(i + local_x - edge) + (j + local_y - edge) * local_dim_x]
				             * mask_matrix[i + j * mask_width];
			}
		}

		out_matrix[out_index] = out_value;
	}

#endif
}
