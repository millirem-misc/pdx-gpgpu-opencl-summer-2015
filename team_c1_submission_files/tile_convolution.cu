/*
 * The purpose of this program is to measure execution speed differences between CUDA and OpenCL.
 *
 * This program was created for the GPU computing class at Portland State University
 *
 * Authors:
 *   Brian Hoff
 *   Byron Marohn
 *   Jim Miller
 *   Yasodhadevi Nachimuthu
 *
 * Compiling:
 *   This program requires the following compiler defined constants:
 *
 *   One of the following defines must be set or compilation will abort:
 *     CONVO_CUDA
 *      or
 *     CONVO_OPENCL
 *
 *   One of these defines may also be set:
 *     CONVO_TYPE_INT       (default)
 *     CONVO_TYPE_FLOAT
 *
 *   This define can be set to suppress all textual program output for benchmarking:
 *     CONVO_SILENT
 */

#include <stdio.h>  // for printf
#include <string.h> // for memcmp
#include <stdlib.h> // for malloc,rand,srand
#include <getopt.h> // for option parsing
#include <limits.h> // for INT_MIN
#include <errno.h>  // for errno/strerror
#include <time.h>   // for time()
#include "time_profiling.h"


/******************************************************************************
 * Preprocessor define parsing
 ******************************************************************************/

#if !defined(CONVO_TYPE_INT) && !defined(CONVO_TYPE_FLOAT)
#define CONVO_TYPE_INT
#endif

#if defined(CONVO_TYPE_INT)
typedef int DATA_TYPE;
#define CONVO_TYPE_FORMAT "%12d"
#define CONVO_OPENCL_TYPE_DEF "CONVO_TYPE_INT"
#elif defined (CONVO_TYPE_FLOAT)
#define CONVO_TYPE_FORMAT "%12f"
typedef float DATA_TYPE;
#define CONVO_OPENCL_TYPE_DEF "CONVO_TYPE_FLOAT"
#endif

#if defined(SIMPLE_CONVOLUTION)
#define CONVO_ALGO "SIMPLE_CONVOLUTION"
#endif

#if defined(SIMPLE_CONVOLUTION_WITH_NAIVE_TILING)
#define CONVO_ALGO "SIMPLE_CONVOLUTION_WITH_NAIVE_TILING"
#endif

#if defined(TILED_CONVOLUTION)
#define CONVO_ALGO "TILED_CONVOLUTION"
#endif

#if defined(CONVO_TILE_THREADS)
#define CONVO_ALGO "CONVO_TILE_THREADS"
#endif

#if defined(CONVO_SILENT)
#define printf(...)
#endif


/******************************************************************************
 * Language-specific includes
 ******************************************************************************/
#if defined(CONVO_CUDA) // CUDA
#include "tile_convolution_kernel.h"

#elif defined(CONVO_OPENCL) // OpenCL
#include <CL/opencl.h>

#else // Stop compilation here if neither of the above matches
#error "Must define either CONVO_CUDA or CONVO_OPENCL"
#endif


/******************************************************************************
 * Device specific global data
 ******************************************************************************/
#ifdef CONVO_OPENCL
const char        *g_cl_source_file = "tile_convolution_kernel.h";
cl_platform_id     g_platform;
cl_device_id       g_device_id;
cl_context         g_context;
cl_command_queue   g_command_queue;
cl_program         g_program;
cl_kernel          g_kernel;
char              *g_cl_source = NULL;
size_t             g_cl_source_len;

/**
  * Allocates a new buffer and reads a file into it.
  * Caller must free the returned buffer when it is done using it.
  */
static char *read_kernel(const char *const filepath, size_t *const filesize)
{
	// Open the file for reading in binary mode
	FILE *f = fopen(filepath, "rb");
	if (f == NULL) {
		fprintf(stderr, "Opening kernel file %s failed\n", filepath);
		exit(EXIT_FAILURE);
	}

	// Measure the file size
	fseek(f, 0, SEEK_END);
	*filesize = ftell(f);
	fseek(f, 0, SEEK_SET);

	// Malloc space for program contents
	char *opencl_program = malloc(*filesize + 1);
	if (opencl_program == NULL) {
		fprintf(stderr, "Error allocating %zd bytes for OpenCL program source\n", *filesize);
		exit(EXIT_FAILURE);
	}

	// Read the source file
	fread(opencl_program, *filesize, 1, f);
	fclose(f);

	// Ensure NULL termination
	opencl_program[*filesize] = '\0';

	return opencl_program;
}
#endif

/******************************************************************************
 * Matrix manipulation code
 ******************************************************************************/

void fill_data(DATA_TYPE *in_matrix, int width, int height, char gen_sequential)
{
	int i;
	for (i = 0; i < width * height; i++) {
		if (gen_sequential) {
			in_matrix[i] = i;
		} else {
#if defined(CONVO_TYPE_INT)
			in_matrix[i] = ((int) rand()) >> 20;
#elif defined(CONVO_TYPE_FLOAT)
			in_matrix[i] = ((float)rand()) / ((float)rand());
#endif
		}
	}
}

void print_data(DATA_TYPE *in_matrix, int width, int height)
{
	int x, y;
	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			//printf("    %d", in_matrix[y * width + x]);
			printf("\t" CONVO_TYPE_FORMAT, in_matrix[y * width + x]);
		}
		printf("\n");
	}
	printf("-----\n");
}

/******************************************************************************
 * GPU Convolution code
 ******************************************************************************/

#if defined(CONVO_OPENCL)
///////////////////////////////////////////////
// copied from here for OpenCL error codes:
// http://stackoverflow.com/questions/24326432/convenient-way-to-show-opencl-error-codes
////////////////////////////////////////////////
const char *oclGetErrorString(cl_int error)
{
  switch(error){
    // run-time and JIT compiler errors
  case 0: return "CL_SUCCESS";
  case -1: return "CL_DEVICE_NOT_FOUND";
  case -2: return "CL_DEVICE_NOT_AVAILABLE";
  case -3: return "CL_COMPILER_NOT_AVAILABLE";
  case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
  case -5: return "CL_OUT_OF_RESOURCES";
  case -6: return "CL_OUT_OF_HOST_MEMORY";
  case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
  case -8: return "CL_MEM_COPY_OVERLAP";
  case -9: return "CL_IMAGE_FORMAT_MISMATCH";
  case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
  case -11: return "CL_BUILD_PROGRAM_FAILURE";
  case -12: return "CL_MAP_FAILURE";
  case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
  case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
  case -15: return "CL_COMPILE_PROGRAM_FAILURE";
  case -16: return "CL_LINKER_NOT_AVAILABLE";
  case -17: return "CL_LINK_PROGRAM_FAILURE";
  case -18: return "CL_DEVICE_PARTITION_FAILED";
  case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
  case -30: return "CL_INVALID_VALUE";
  case -31: return "CL_INVALID_DEVICE_TYPE";
  case -32: return "CL_INVALID_PLATFORM";
  case -33: return "CL_INVALID_DEVICE";
  case -34: return "CL_INVALID_CONTEXT";
  case -35: return "CL_INVALID_QUEUE_PROPERTIES";
  case -36: return "CL_INVALID_COMMAND_QUEUE";
  case -37: return "CL_INVALID_HOST_PTR";
  case -38: return "CL_INVALID_MEM_OBJECT";
  case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
  case -40: return "CL_INVALID_IMAGE_SIZE";
  case -41: return "CL_INVALID_SAMPLER";
  case -42: return "CL_INVALID_BINARY";
  case -43: return "CL_INVALID_BUILD_OPTIONS";
  case -44: return "CL_INVALID_PROGRAM";
  case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
  case -46: return "CL_INVALID_KERNEL_NAME";
  case -47: return "CL_INVALID_KERNEL_DEFINITION";
  case -48: return "CL_INVALID_KERNEL";
  case -49: return "CL_INVALID_ARG_INDEX";
  case -50: return "CL_INVALID_ARG_VALUE";
  case -51: return "CL_INVALID_ARG_SIZE";
  case -52: return "CL_INVALID_KERNEL_ARGS";
  case -53: return "CL_INVALID_WORK_DIMENSION";
  case -54: return "CL_INVALID_WORK_GROUP_SIZE";
  case -55: return "CL_INVALID_WORK_ITEM_SIZE";
  case -56: return "CL_INVALID_GLOBAL_OFFSET";
  case -57: return "CL_INVALID_EVENT_WAIT_LIST";
  case -58: return "CL_INVALID_EVENT";
  case -59: return "CL_INVALID_OPERATION";
  case -60: return "CL_INVALID_GL_OBJECT";
  case -61: return "CL_INVALID_BUFFER_SIZE";
  case -62: return "CL_INVALID_MIP_LEVEL";
  case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
  case -64: return "CL_INVALID_PROPERTY";
  case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
  case -66: return "CL_INVALID_COMPILER_OPTIONS";
  case -67: return "CL_INVALID_LINKER_OPTIONS";
  case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
  case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
  case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
  case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
  case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
  case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
  case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
  default: return "Unknown OpenCL error";
  }

 return "Unknown OpenCL error";
}
#endif

#define CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status) \
	{ \
		if (status != CL_SUCCESS) { \
			fprintf(stderr, "ERROR: Status: %s - (code %d) at Line %u in file %s\n\n", oclGetErrorString(status), status, __LINE__, __FILE__); \
			goto opencl_exit; \
		} \
	}

// NOTE: Large sections of the OpenCL code in this function were derived
//       from the oclVectorAdd.cpp file in the CUDA OpenCL SDK
void convolute_dev(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix,
                   int width, int height, int mask_width, int tile_width)
{
	////////////////////////////////////////////////
	// 1 - Device initialization
	////////////////////////////////////////////////
	BENCHMARK_INIT();
	BENCHMARK_START();
#if defined(CONVO_CUDA)
	// No cuda initialization code here

#elif defined(CONVO_OPENCL)
	cl_int status;

	//Get an OpenCL platform
	status = clGetPlatformIDs(1, &g_platform, NULL);
	printf("clGetPlatformID...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	//Get the devices
	status = clGetDeviceIDs(g_platform, CL_DEVICE_TYPE_GPU, 1, &g_device_id, NULL);
	printf("clGetDeviceIDs...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	//Create the context
	g_context = clCreateContext(0, 1, &g_device_id, NULL, NULL, &status);
	printf("clCreateContext...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	// Create a command-queue
	g_command_queue = clCreateCommandQueue(g_context, g_device_id, 0, &status);
	printf("clCreateCommandQueue...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	// Read the OpenCL kernel in from source file
	printf("read_kernel (%s)...\n", g_cl_source_file);
	g_cl_source = read_kernel(g_cl_source_file, &g_cl_source_len);

	// Create the program
	g_program = clCreateProgramWithSource(g_context, 1, (const char **)&g_cl_source,
	                                      &g_cl_source_len, &status);
	printf("clCreateProgramWithSource...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	// Build the program with CONVO_OPENCL defined"
	char flags[1024] = "";
	snprintf(flags, sizeof(flags), "-DCONVO_OPENCL -D%s -D%s", CONVO_OPENCL_TYPE_DEF, CONVO_ALGO);
	status = clBuildProgram(g_program, 0, NULL, flags, NULL, NULL);
	printf("clBuildProgram...\n");
	if (status != CL_SUCCESS) {
		// If compilation fails, need to print the log messages
		printf("clBuildProgram failed, printing compiler errors...\n\n\n");

		// Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(g_program, g_device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// Allocate memory for the log
		char *log = (char *) malloc(log_size);

		// Get the log
		clGetProgramBuildInfo(g_program, g_device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// Print the log
		printf("%s\n\n\n", log);
	}
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	// Create the kernel
	g_kernel = clCreateKernel(g_program, "convolute_kernel", &status);
	printf("clCreateKernel (VectorAdd)...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);
#endif
	BENCHMARK_STOP("setup");

	////////////////////////////////////////////////
	// 2 - Memory Allocation
	////////////////////////////////////////////////
	BENCHMARK_START();
#if defined(CONVO_CUDA)
	DATA_TYPE *in_matrix_dev, *out_matrix_dev, *mask_matrix_dev;
	cudaMalloc((void **) &in_matrix_dev, sizeof(DATA_TYPE) * (width * height));
	cudaMalloc((void **) &out_matrix_dev, sizeof(DATA_TYPE) * (width * height));
	cudaMalloc((void **) &mask_matrix_dev, sizeof(DATA_TYPE) * (mask_width * mask_width));
#elif defined(CONVO_OPENCL)
	cl_mem in_matrix_dev, out_matrix_dev, mask_matrix_dev;
	cl_int status_accumulator = CL_SUCCESS;

	in_matrix_dev = clCreateBuffer(g_context, CL_MEM_READ_ONLY,
	                               sizeof(DATA_TYPE) * (width * height), NULL, &status);
	status_accumulator |= status;
	out_matrix_dev = clCreateBuffer(g_context, CL_MEM_WRITE_ONLY,
	                                sizeof(DATA_TYPE) * (width * height), NULL, &status);
	status_accumulator |= status;
	mask_matrix_dev = clCreateBuffer(g_context, CL_MEM_READ_ONLY,
	                                 sizeof(DATA_TYPE) * (mask_width * mask_width), NULL, &status);
	status_accumulator |= status;

	printf("clCreateBuffer...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);
#endif
	BENCHMARK_STOP("malloc");

	////////////////////////////////////////////////
	// 3 - Memory Copy To Device
	////////////////////////////////////////////////
	BENCHMARK_START();
#if defined(CONVO_CUDA)
	cudaMemcpy(in_matrix_dev, in_matrix, sizeof(DATA_TYPE) * (width * height), cudaMemcpyHostToDevice);
	cudaMemcpy(mask_matrix_dev, mask_matrix, sizeof(DATA_TYPE) * (mask_width * mask_width),
	           cudaMemcpyHostToDevice);
#elif defined(CONVO_OPENCL)
	// Synchronous write of data to GPU device
	status = clEnqueueWriteBuffer(g_command_queue, in_matrix_dev, CL_TRUE, 0,
	                              sizeof(DATA_TYPE) * (width * height), in_matrix, 0, NULL, NULL);
	status |= clEnqueueWriteBuffer(g_command_queue, mask_matrix_dev, CL_TRUE, 0,
	                               sizeof(DATA_TYPE) * (mask_width * mask_width), mask_matrix, 0, NULL, NULL);
	printf("clEnqueueWriteBuffer...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);
#endif
	BENCHMARK_STOP("memcpy");

	////////////////////////////////////////////////
	// 4 - Compute work dimensions
	////////////////////////////////////////////////
#if defined(CONVO_CUDA)
	int block_x, block_y;
#if defined(CONVO_TILE_THREADS)
	uint offset = tile_width - mask_width + 1;
	block_x = width / offset;
	block_y = height / offset;

	if (width % offset != 0) block_x++;
	if (height % offset != 0) block_y++;
#else
	block_x = width / tile_width;
	if (width % tile_width != 0)  block_x++;
	block_y = height / tile_width;
	if (height % tile_width != 0) block_y++;
#endif
	printf("offset: %u\n"
	       "grid: [%d, %d]\n"
	       "block: [%d, %d]\n",
	       offset,
	       block_x, block_y,
	       tile_width, tile_width);

	dim3 dimGrid(block_x, block_y,  1);
	dim3 dimBlock(tile_width, tile_width, 1);
#elif defined(CONVO_OPENCL)
#if defined(CONVO_TILE_THREADS)
	size_t offset = tile_width - mask_width + 1;

	size_t w = width / offset + ((offset % width == 0) ? 0 : 1);
	size_t h = height / offset + ((offset % height == 0) ? 0 : 1);

	size_t global_work_size[2] = {w * tile_width, h * tile_width};
	size_t local_work_size[2] = {tile_width, tile_width};

	printf("w: %zu\th: %zu\n"
	       "global_work_size: [%zu, %zu]\n"
	       "local_work_size: [%zu, %zu]\n",
	       w, h,
	       global_work_size[0], global_work_size[1],
	       local_work_size[0], local_work_size[1]);

#else
	size_t global_work_size[2] = {width, height};
	size_t local_work_size[2] = {tile_width, tile_width};
#endif
#endif

	////////////////////////////////////////////////
	// 5 - Run kernel
	////////////////////////////////////////////////
	BENCHMARK_START();
#if defined(CONVO_TILE_THREADS)
	int shared_mem_size = sizeof(DATA_TYPE) * tile_width * tile_width;
#else
	int shared_mem_size = sizeof(DATA_TYPE) * (tile_width + mask_width - 1)
	                      * (tile_width + mask_width - 1);
#endif

#if defined(CONVO_CUDA)
	convolute_kernel <<< dimGrid, dimBlock, shared_mem_size>>>(out_matrix_dev, in_matrix_dev,
	                mask_matrix_dev, width,
	                height, mask_width);
#elif defined(CONVO_OPENCL)
	// Set the Argument values
	status = clSetKernelArg(g_kernel, 0, sizeof(cl_mem), (void *)&out_matrix_dev);
	status |= clSetKernelArg(g_kernel, 1, sizeof(cl_mem), (void *)&in_matrix_dev);
	status |= clSetKernelArg(g_kernel, 2, sizeof(cl_mem), (void *)&mask_matrix_dev);
	status |= clSetKernelArg(g_kernel, 3, sizeof(cl_int), (void *)&width);
	status |= clSetKernelArg(g_kernel, 4, sizeof(cl_int), (void *)&height);
	status |= clSetKernelArg(g_kernel, 5, sizeof(cl_int), (void *)&mask_width);
	status |= clSetKernelArg(g_kernel, 6, shared_mem_size, NULL);
	printf("clSetKernelArg...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);

	status = clFinish(g_command_queue);
	// Launch kernel
	status |= clEnqueueNDRangeKernel(g_command_queue, g_kernel, 2, NULL, global_work_size,
	                                 local_work_size, 0, NULL, NULL);
	printf("clEnqueueNDRangeKernel (convolute_kernel)...\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);
#endif

	////////////////////////////////////////////////
	// 5.1 - Wait for kernel to complete
	////////////////////////////////////////////////
#if defined(CONVO_CUDA)
	cudaDeviceSynchronize();
#elif defined(CONVO_OPENCL)
	status = clFinish(g_command_queue);
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);
#endif

	BENCHMARK_STOP("kernel");
	BENCHMARK_FINALIZE();

	////////////////////////////////////////////////
	// 6 - Collect results
	////////////////////////////////////////////////
#if defined(CONVO_CUDA)
	cudaMemcpy(out_matrix, out_matrix_dev, sizeof(DATA_TYPE) * (width * height),
	           cudaMemcpyDeviceToHost);
#elif defined(CONVO_OPENCL)
	// Synchronous/blocking read of results, and check accumulated errors
	status = clEnqueueReadBuffer(g_command_queue, out_matrix_dev, CL_TRUE, 0,
	                             sizeof(DATA_TYPE) * (width * height), out_matrix, 0, NULL, NULL);
	printf("clEnqueueReadBuffer...\n\n");
	CONVOLUTE_DEV_CHECK_OPENCL_STATUS(status);
#endif


	////////////////////////////////////////////////
	// 7 - Cleanup
	////////////////////////////////////////////////
#if defined(CONVO_CUDA)
	cudaFree(in_matrix_dev);
	cudaFree(mask_matrix_dev);
	cudaFree(out_matrix_dev);
#elif defined(CONVO_OPENCL)
opencl_exit:
	printf("Starting exit...\n\n");
	//if(cPathAndName)free(cPathAndName);
	if (g_cl_source) free(g_cl_source);
	if (g_kernel) clReleaseKernel(g_kernel);
	if (g_program) clReleaseProgram(g_program);
	if (g_command_queue) clReleaseCommandQueue(g_command_queue);
	if (in_matrix_dev) clReleaseMemObject(in_matrix_dev);
	if (out_matrix_dev) clReleaseMemObject(out_matrix_dev);
	if (mask_matrix_dev) clReleaseMemObject(mask_matrix_dev);
	if (g_context) clReleaseContext(g_context);
	if (status) {
		fprintf(stderr, "OpenCL exiting due to non-zero status\n");
		exit(EXIT_FAILURE);
	}
#endif
}

/******************************************************************************
 * CPU Convolution Code
 ******************************************************************************/

void convolute(DATA_TYPE *out_matrix, DATA_TYPE *in_matrix, const DATA_TYPE *mask_matrix,
               int width, int height, int mask_width)
{
	int x, y;
	int out_index;
	int in_x_start, in_y_start;
	DATA_TYPE out_value;
	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			out_index = (y * width) + x;
			in_x_start = x - mask_width / 2;
			in_y_start = y - mask_width / 2;
			out_value = 0;
			//printf("x start is %d , y start is %d \n",in_x_start, in_y_start);
			for (int j = 0; j < mask_width; j++) {
				for (int k = 0; k < mask_width; k++) {
					if (in_x_start + k >= 0 && in_y_start + j >= 0 && in_x_start + k < width
					    && in_y_start + j < height) {
						out_value += in_matrix[((in_y_start + j) * width) + in_x_start + k]
						             * mask_matrix[j * mask_width + k];
					}
				}
			}
			out_matrix[out_index] = out_value;
		}
	}
}


/******************************************************************************
 * Program Entry
 ******************************************************************************/

void print_usage(char *progname)
{
	fprintf(stderr,
	        "Usage: %s [arguments]\n"
	        "\n"
	        "This program illustrates the difference between CUDA and OpenCL GPU matrix convolution\n"
	        "Created for the 2015 GPU Acceleration summer course at Portland State University\n"
	        "\n"
	        "Authors: \n"
	        "  Brian Hoff\n"
	        "  Byron Marohn\n"
	        "  Jim Miller\n"
	        "  Yasodhadevi Nachimuthu\n"
	        "\n"
#if defined(CONVO_CUDA)
	        "This is the CUDA version of tiled convolution\n"
#elif defined(CONVO_OPENCL)
	        "This is the OpenCL version of tiled convolution\n"
#endif
	        "\n"
	        "If no arguments are specified default values will be used and new matrices are generated\n"
	        "Note that string arguments are limited to 255 characters\n"
	        "\n"
	        "Avalaible arguments:\n"
	        "  --input-matrix <matrix_file>\n"
	        "    Specifies the input matrix file to load. The first line is width, height.\n"
	        "    Spaces are allowed to make things readable, and are ignored by the parser.\n"
	        "    For example:\n"
	        "      5,3\n"
	        "      12,-2,8,16,1\n"
	        "      1 , 2,3,4 ,5\n"
	        "      6 , 7,8,9 ,10\n"
	        "  --tile-width <width>\n"
	        "    Specifies the tile width to use. Values allowed are restricted to valid CUDA\n"
	        "    block widths\n"
	        "  --input-mask <mask_file>\n"
	        "    Specifies the mask to load. Same format as the input matrix\n"
	        "\n"
	        "The following arguments are useful for automatically generating input files:\n"
	        "  --gen-input <output_file>\n"
	        "    Generates a valid input matrix file and then exits\n"
	        "    Can generate both input and mask in one program run.\n"
	        "    Use --width and --height to specify dimensions\n"
	        "  --width <width>\n"
	        "    Can be specified with --input-matrix to load a subset matrix with smaller width\n"
	        "  --height <height>\n"
	        "    Can be specified with --input-matrix to load a subset matrix with smaller height\n"
	        "  --gen-mask <output_file>\n"
	        "    Generates a valid mask matrix file and then exits.\n"
	        "    Can generate both input and mask in one program run.\n"
	        "    Use --mask-width to specify size\n"
	        "  --mask-width <width>\n"
	        "    Can be specified with --input-mask to load a subset matrix with smaller width\n"
	        "  --gen-sequential\n"
	        "    Generate sequential instead of random input data\n",
	        progname);
	exit(EXIT_FAILURE);
}

void save_matrix(char *filename, int width, int height, DATA_TYPE *matrix)
{
	FILE *fp = NULL;
	int i, j;

	fp = fopen(filename, "wb");
	if (fp == NULL) {
		fprintf(stderr, "Error opening file %s: %s\n", filename, strerror(errno));
		return;
	}

	fprintf(fp, "%d, %d\n", width, height);

	for (j = 0; j < height; j++) {
		i = 0;
		fprintf(fp, CONVO_TYPE_FORMAT, matrix[j * width + i]);
		for (i = 1; i < width; i++) {
			fprintf(fp, ", " CONVO_TYPE_FORMAT, matrix[j * width + i]);
		}
		fprintf(fp, "\n");
	}

	fclose(fp);

	printf("Generated matrix written to %s successfully\n", filename);
}

/*
 * Loads a file into a matrix and returns it. If width and height are greater than zero, they are
 *   used to load a subset of the supplied matrix. If they are less than 1, dimensions are loaded
 *   from the first line of the file.
 *
 * Note that dimensions are required in the file, even if they are overridden.
 */
DATA_TYPE *load_matrix(char *filename, int *width, int *height)
{
	FILE *fp = NULL;
	int filewidth, fileheight;
	int i, j, status, val;
	DATA_TYPE *ret_matrix = NULL;
	char buf[256];
	unsigned int bufptr;

	// Open the file and read the dimensions
	fp = fopen(filename, "rb");
	if (fp == NULL) {
		fprintf(stderr, "Error opening file %s: %s\n", filename, strerror(errno));
		goto error_exit;
	}
	status = fscanf(fp, "%d,%d", &filewidth, &fileheight);
	if (status != 2) {
		fprintf(stderr, "Error reading dimensions from file %s\n", filename);
		printf("status: %d\n", status);
		goto error_exit;
	}

	// Override dimensions with passed-in values if they were supplied
	filewidth = (*width < 1 ? filewidth : *width);
	fileheight = (*height < 1 ? fileheight : *height);
	*width = filewidth;
	*height = fileheight;

	// Make sure resulting dimensions are correct
	if (filewidth < 1) {
		printf("Error parsing %s: width must be greater than zero, got %d\n", filename, filewidth);
		goto error_exit;
	}
	if (fileheight < 1) {
		printf("Error parsing %s: height must be greater than zero, got %d\n", filename, fileheight);
		goto error_exit;
	}

	// Allocate the appropriate amount of memory
	ret_matrix = (DATA_TYPE *)malloc(sizeof(DATA_TYPE) * (filewidth * fileheight));
	if (ret_matrix == NULL) {
		fprintf(stderr, "Allocation of (%d,%d) matrix failed, perhaps it is too big?\n", filewidth,
		        fileheight);
		return NULL;
	}

	// Advance to the end of the first line
	val = '\0';
	while (val != '\n') {
		val = fgetc(fp);
		if (val == EOF) {
			fprintf(stderr, "Unexpected end of file while reading %s using dimensions %d,%d\n",
			        filename, filewidth, fileheight);
			goto error_exit;
		}
	}

	for (j = 0; j < fileheight; j++) {
		val = '\0';
		for (i = 0; i < filewidth; i++) {
			bufptr = 0;
			buf[bufptr] = '\0';
			buf[sizeof(buf) - 1] = '\0';

			while (bufptr < (sizeof(buf) - 1)) {
				val = fgetc(fp);
				if (val == EOF) {
					fprintf(stderr, "Unexpected end of file while reading %s using dimensions %d,%d\n",
					        filename, filewidth, fileheight);
					goto error_exit;
				} else if ((val >= '0' && val <= '9') || val == '-' || val == '.') {
					// Value is a number, copy it over to the parsing buffer
					buf[bufptr] = (char) val;
					bufptr++;
				} else if (val == '\n' && i < filewidth - 1) {
					// Got an end of line that wasn't at the expected end of the x values
					fprintf(stderr, "Unexpected end of line at location %d,%d in input file %s\n",
					        i, j, filename);
					goto error_exit;
				} else if (bufptr > 0) {
					// Got a non-number after getting some numbers. Stop and parse that number.
					buf[bufptr] = '\0';
					status = sscanf(buf, CONVO_TYPE_FORMAT, &ret_matrix[j * filewidth + i]);
					if (status != 1) {
						fprintf(stderr, "Error reading integer value at location %d,%d in input file %s\n",
						        i, j, filename);
						goto error_exit;
					}
					break;
				}
			}
		}

		// If the last thing read wasn't a newline, need to advance to the next newline
		while (val != '\n') {
			val = fgetc(fp);
			if (val == EOF) {
				fprintf(stderr, "Unexpected end of file while reading %s using dimensions %d,%d\n",
				        filename, filewidth, fileheight);
				goto error_exit;
			}
		}
	}

	fclose(fp);

	return ret_matrix;

error_exit:
	if (ret_matrix != NULL) free(ret_matrix);
	if (fp != NULL) fclose(fp);
	return NULL;
}


int main(int argc, char **argv)
{
	DATA_TYPE *in_matrix = NULL;
	DATA_TYPE *out_matrix_host = NULL;
	DATA_TYPE *out_matrix_dev = NULL;
	DATA_TYPE *mask_matrix = NULL;

	int status = EXIT_SUCCESS;
	int i;

	////////////////////////////////////////////////
	// Default values
	////////////////////////////////////////////////

	int width = 8;
	int height = 8;
	int tile_width = 8;
	int mask_width = 5;

	////////////////////////////////////////////////
	// Argument Parsing
	////////////////////////////////////////////////

	char arg_input_matrix[256] = "";
	int  arg_tile_width        = INT_MIN;
	char arg_input_mask[256]   = "";
	char arg_gen_input[256]    = "";
	int  arg_width             = INT_MIN;
	int  arg_height            = INT_MIN;
	char arg_gen_mask[256]     = "";
	int  arg_mask_width        = INT_MIN;
	char arg_gen_sequential    = 0;

	static struct option long_options[] = {
		{"input-matrix", required_argument, 0,  'i' },
		{"tile-width",   required_argument, 0,  't' },
		{"input-mask",   required_argument, 0,  'x' },
		{"gen-input",    required_argument, 0,  'g' },
		{"width",        required_argument, 0,  'w' },
		{"height",       required_argument, 0,  'h' },
		{"gen-mask",     required_argument, 0,  'k' },
		{"mask-width",   required_argument, 0,  'm' },
		{"gen-sequential",  no_argument,    0,  's' },
		{0,              0,                 0,  0   }
	};

	int long_index = 0;
	int opt;
	while ((opt = getopt_long(argc, argv, "si:t:x:g:w:h:k:m:",
	                          long_options, &long_index)) != -1) {
		switch (opt) {
		case 'i' :
			strncpy(arg_input_matrix, optarg, sizeof(arg_input_matrix) - 1);
			arg_input_matrix[sizeof(arg_input_matrix) - 1] = '\0';
			break;
		case 't' :
			arg_tile_width = atoi(optarg);
			break;
		case 'x' :
			strncpy(arg_input_mask, optarg, sizeof(arg_input_mask) - 1);
			arg_input_mask[sizeof(arg_input_mask) - 1] = '\0';
			break;
		case 'g' :
			strncpy(arg_gen_input, optarg, sizeof(arg_gen_input) - 1);
			arg_gen_input[sizeof(arg_gen_input) - 1] = '\0';
			break;
		case 'w' :
			arg_width = atoi(optarg);
			break;
		case 'h' :
			arg_height = atoi(optarg);
			break;
		case 'k' :
			strncpy(arg_gen_mask, optarg, sizeof(arg_gen_mask) - 1);
			arg_gen_mask[sizeof(arg_gen_mask) - 1] = '\0';
			break;
		case 'm' :
			arg_mask_width = atoi(optarg);
			break;
		case 's' :
			arg_gen_sequential = 1;
			break;
		default:
			print_usage(argv[0]);
			status = EXIT_FAILURE;
			goto cleanup_exit;
		}
	}

	if (arg_tile_width > 0) {
		tile_width = arg_tile_width;
	} else if (arg_tile_width != INT_MIN) {
		printf("Tile width must be greater than zero, got %d\n", arg_tile_width);
		status = EXIT_FAILURE;
		goto cleanup_exit;
	}
	if (arg_width > 0) {
		width = arg_width;
	} else if (arg_width != INT_MIN) {
		printf("Width must be greater than zero, got %d\n", arg_width);
		status = EXIT_FAILURE;
		goto cleanup_exit;
	}
	if (arg_height > 0) {
		height = arg_height;
	} else if (arg_height != INT_MIN) {
		printf("Height must be greater than zero, got %d\n", arg_height);
		status = EXIT_FAILURE;
		goto cleanup_exit;
	}
	if (arg_mask_width > 0 && (arg_mask_width % 2) == 1) {
		mask_width = arg_mask_width;
	} else if (arg_mask_width != INT_MIN) {
		printf("Mask width must be greater than zero and odd, got %d\n", arg_mask_width);
		status = EXIT_FAILURE;
		goto cleanup_exit;
	}

	// Seed the random number generator with the current time
	srand(time(NULL));

	// If the matrix is going to be saved or not loaded from file, it must be generated
	if ((strlen(arg_gen_input) > 0) || (strlen(arg_input_matrix) == 0)) {
		in_matrix = (DATA_TYPE *)malloc(sizeof(DATA_TYPE) * (width * height));
		fill_data(in_matrix, width, height, arg_gen_sequential);
	}
	// If the mask matrix is going to be saved or not loaded from file, it must be generated
	if ((strlen(arg_gen_mask) > 0) || (strlen(arg_input_mask) == 0)) {
		mask_matrix = (DATA_TYPE *)malloc(sizeof(DATA_TYPE) * (mask_width * mask_width));
		fill_data(mask_matrix, mask_width, mask_width, arg_gen_sequential);
	}

	// Save generated matrices if requested
	if (strlen(arg_gen_input) > 0) {
		save_matrix(arg_gen_input, width, height, in_matrix);
	}
	if (strlen(arg_gen_mask) > 0) {
		save_matrix(arg_gen_mask, mask_width, mask_width, mask_matrix);
	}
	if ((strlen(arg_gen_input) > 0) || (strlen(arg_gen_mask) > 0)) {
		status = EXIT_SUCCESS;
		goto cleanup_exit;
	}

	// Load input matrix and mask if specified. Loader will allocate memory.
	if (strlen(arg_input_matrix) > 0) {
		if (arg_width > 0) {
			printf("WARN: Overriding input file width with --width value %d\n", arg_width);
			width = arg_width;
		} else {
			width = -1;
		}
		if (arg_height > 0) {
			printf("WARN: Overriding input file height with --height value %d\n", arg_height);
			height = arg_height;
		} else {
			height = -1;
		}

		in_matrix = load_matrix(arg_input_matrix, &width, &height);
		if (in_matrix == NULL) {
			status = EXIT_FAILURE;
			goto cleanup_exit;
		}
	}
	if (strlen(arg_input_mask) > 0) {
		if (arg_mask_width > 0) {
			printf("WARN: Overriding mask file dimensions with --mask-width value %d\n", arg_mask_width);
			mask_width = arg_mask_width;
		} else {
			mask_width = -1;
		}
		mask_matrix = load_matrix(arg_input_mask, &mask_width, &mask_width);
		if (mask_matrix == NULL) {
			status = EXIT_FAILURE;
			goto cleanup_exit;
		}
		if (mask_width <= 0 || (mask_width % 2) == 0) {
			printf("Mask width must be greater than zero and odd, got %d\n", mask_width);
			status = EXIT_FAILURE;
			goto cleanup_exit;
		}
	}

	printf("Tile width set to %d\n", tile_width);

	////////////////////////////////////////////////
	// Convolution
	////////////////////////////////////////////////

	out_matrix_host = (DATA_TYPE *)malloc(sizeof(DATA_TYPE) * (width * height));
	out_matrix_dev = (DATA_TYPE *)malloc(sizeof(DATA_TYPE) * (width * height));

	printf("Input matrix:\n");
	print_data(in_matrix, width, height);

	printf("Mask matrix:\n");
	print_data(mask_matrix, mask_width, mask_width);

	convolute(out_matrix_host, in_matrix, mask_matrix, width, height, mask_width);
	printf("CPU done\n");
	print_data(out_matrix_host, width, height);

	convolute_dev(out_matrix_dev, in_matrix, mask_matrix, width, height, mask_width,
	              tile_width);
	printf("GPU done\n");
	print_data(out_matrix_dev, width, height);

	for (i = 0; i < width * height; i++) {
#if defined(CONVO_TYPE_INT)
		// Integer convolution requires a straightforward comparison
		if (out_matrix_host[i] != out_matrix_dev[i]) {
#elif defined(CONVO_TYPE_FLOAT)
		// Floating point engines differ in precision, only check that values
		//   are within 1/10000th of eachother (.01%)
		if ((out_matrix_host[i] * 1.0001 < out_matrix_dev[i])
		    || (out_matrix_host[i] * 0.9999 > out_matrix_dev[i])) {
#endif
			fprintf(stderr, "Error: Host and GPU differ\n");
			status = EXIT_FAILURE;
			goto cleanup_exit;
		}
	}
	fprintf(stderr, "Success: Host and GPU computed same results\n");
	status = EXIT_SUCCESS;

cleanup_exit:
	if (in_matrix)       free(in_matrix);
	if (mask_matrix)     free(mask_matrix);
	if (out_matrix_host) free(out_matrix_host);
	if (out_matrix_dev)  free(out_matrix_dev);

	return status;
}
